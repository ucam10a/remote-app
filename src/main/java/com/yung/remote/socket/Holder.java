package com.yung.remote.socket;

import java.io.IOException;

import com.yung.remote.MarshalHelper;
import com.yung.remote.MySecurityManager;

public class Holder {

    private int part;
    
    private String fileName;
    
    private int total;
    
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPart() {
        return part;
    }

    public void setPart(int part) {
        this.part = part;
    }

    public static String marshal(Holder holder) throws IOException {
        MarshalHelper helper = new MarshalHelper();
        String content = helper.marshal("holder", holder);
        return MySecurityManager.compress(content);
    }
    
    public static Holder unmarshal(String holderText) throws IOException {
        String text = MySecurityManager.decompress(holderText);
        MarshalHelper helper = new MarshalHelper();
        Holder holder = helper.unmarshal(text, Holder.class);
        return holder;
    }
    
}
