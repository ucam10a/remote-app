package com.yung.remote.socket;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.yung.remote.App;
import com.yung.remote.FileUtil;
import com.yung.remote.MyJDialog;
import com.yung.remote.MySecurityManager;

public class Client {

    private long FILE_SIZE_LIMIT = 10 * 1024 * 1024;
    private App app;
    private MyJDialog dialog;
    
    public Client (App app, MyJDialog dialog) {
        this.app = app;
        this.dialog = dialog;
    }
    
    public void call(final String function, final Object... params) {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(new Runnable() {
            public void run() {
                if ("connectToServer".equals(function)) {
                    connectToServer();
                } else if ("snedMsg".equals(function)) {
                    snedMsg(params[0].toString(), params[1].toString());
                } else if ("alert".equals(function)) {
                    alert();
                } else if ("sendFile".equals(function)) {
                    sendFile(params[0].toString(), params[1].toString());
                } 
            }
        });
        executorService.shutdown();
    }
    
    private void connectToServer() {
        String clientIp = app.getIpField().getText();
        String pass = app.getPwdField().getText();
        if (clientIp == null || "".equals(clientIp.trim())) {
            app.logMessage("Error: ip is null");
            return;
        }
        if (pass == null || "".equals(pass.trim())) {
            app.logMessage("Error: password is null");
            return;
        }
        app.logMessage("send request to server, please wait ...");
        Socket socket = null;
        try {
            String hostIp = InetAddress.getLocalHost().getHostAddress();
            MySecurityManager securityMgr = new MySecurityManager();
            String code = securityMgr.encrypt(hostIp + pass);
            socket = new Socket(clientIp, Server.SOCKET_PORT);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println("code->" + code);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String message = in.readLine();
            if (message == null || "".equals(message.trim())) {
                return;
            }
            app.logMessage(message);
            if (message.equals("accept")) {
                app.logMessage("accept to connect");
                if (app.checkZeroremoteProcess()) {
                    app.breakConnection();
                }
                app.connect();
            } else {
                app.logMessage("reject to connect");
            }
        } catch (Exception e) {
            app.logMessage(e.toString());
            if (dialog != null) dialog.logMessage(e.toString());
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {
                    app.logMessage(e.toString());
                }
            }
        }
    }

    private void snedMsg(String clientIp, String msg) {
        try {
            String base64Msg = MySecurityManager.encodeBase64(msg);
            if (clientIp == null || "".equals(clientIp.trim())) {
                app.logMessage("Error: ip is null");
                return;
            }
            Socket socket = null;
            try {
                socket = new Socket(clientIp, Server.SOCKET_PORT);
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                out.println("msg->" + base64Msg);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String message = in.readLine();
                if (message == null || "".equals(message.trim())) {
                    return;
                }
                app.logMessage(message);
            } catch (Exception e) {
                app.logMessage(e.toString());
                if (dialog != null) dialog.logMessage(e.toString());
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (Exception e) {
                        app.logMessage(e.toString());
                    }
                }
            }
        } catch (Exception e) {
            app.logMessage(e.toString());
            if (dialog != null) dialog.logMessage(e.toString());
        }
    }

    private void alert() {
        String clientIp = app.getIpField().getText();
        if (clientIp == null || "".equals(clientIp.trim())) {
            app.logMessage("Error: ip is null");
            return;
        }
        Socket socket = null;
        try {
            socket = new Socket(clientIp, Server.SOCKET_PORT);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println("alert->");
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String message = in.readLine();
            if (message == null || "".equals(message.trim())) {
                return;
            }
            app.logMessage(message);
        } catch (Exception e) {
            app.logMessage(e.toString());
            if (dialog != null) dialog.logMessage(e.toString());
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {
                    app.logMessage(e.toString());
                }
            }
        }
    }
    
    public boolean checkAlive(String clientIp) {
        Socket socket = null;
        try {
            socket = new Socket(clientIp, Server.SOCKET_PORT);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println("alive->");
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String message = in.readLine();
            if (message == null || "".equals(message.trim())) {
                return true;
            }
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {
                    app.logMessage(e.toString());
                }
            }
        }
    }

    public boolean checkConnect(String clientIp) {
        String ip = clientIp;
        int idx = ip.indexOf(":");
        if (idx > 0) {
            int start = 0;
            if (ip.startsWith("/")) {
                start = 1;
            }
            ip = ip.substring(start, idx).trim();
        }
        Socket socket = null;
        try {
            socket = new Socket(ip, Server.SOCKET_PORT);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println("connect->");
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String result = null;
            while (true) {
                result = in.readLine();
                if (result == null || "".equals(result.trim())) {
                    break;
                }
                break;
            }
            return Boolean.valueOf(result);
        } catch (Exception e) {
            return false;
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {
                    app.logMessage(e.toString());
                }
            }
        }
    }
    
    private void sendFile(String clientIp, String filePath) {
        String ip = clientIp;
        int idx = ip.indexOf(":");
        if (idx > 0) {
            int start = 0;
            if (ip.startsWith("/")) {
                start = 1;
            }
            ip = ip.substring(start, idx).trim();
        }
        try {
            File srcFile = FileUtil.getFile(filePath);
            double length = srcFile.length();
            BigDecimal mb = new BigDecimal((length / (1024 * 1024)));
            mb = mb.setScale(3, BigDecimal.ROUND_HALF_UP);
            if (length > FILE_SIZE_LIMIT) {
                app.logMessage("file size: " + mb + "MB is over limit: " + (FILE_SIZE_LIMIT / (1024 * 1024)) + "MB");
            }
            String base64Text = MySecurityManager.compress(srcFile);
            List<String> parts = split(base64Text);
            boolean success = true;
            for (int i = 0; i < parts.size(); i++) {
                Socket socket = null;
                try {
                    socket = new Socket(ip, Server.SOCKET_PORT);
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    Holder holder = new Holder();
                    holder.setFileName(srcFile.getName());
                    holder.setTotal(parts.size());
                    holder.setPart(i);
                    out.println("file->" + Holder.marshal(holder) + "@" + parts.get(i));
                    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String msg = in.readLine();
                    if ("success".equals(msg)) {
                        app.logMessage("Sending " + srcFile.getName() + " " + ((i + 1) * 100 / parts.size()) + "% ...");
                    } else {
                        app.logMessage("Sending " + srcFile.getName() + " fail");
                        success = false;
                        break;
                    }
                } finally {
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (Exception e) {
                            app.logMessage(e.toString());
                        }
                    }
                }
            }
            joinFile(clientIp, filePath, success);
        } catch (Exception e) {
            app.logMessage(e.toString());
            if (dialog != null) dialog.logMessage(e.toString());
            joinFile(clientIp, filePath, false);
        }
    }

    private List<String> split(String base64Text) {
        int partSize = (base64Text.length() / 10) + 1;
        List<String> result = new ArrayList<String>();
        for (int i = 0; i < 10; i++) {
            int from = i * partSize;
            if (from > base64Text.length()) {
                break;
            }
            int end = (i + 1) * partSize;
            if (end > base64Text.length()) {
                end = base64Text.length();
            }
            result.add(i, base64Text.substring(from, end));
        }
        return result;
    }

    private void joinFile(String clientIp, String filePath, boolean sendDone) {
        String ip = clientIp;
        int idx = ip.indexOf(":");
        if (idx > 0) {
            int start = 0;
            if (ip.startsWith("/")) {
                start = 1;
            }
            ip = ip.substring(start, idx).trim();
        }
        Socket socket = null;
        try {
            File srcFile = FileUtil.getFile(filePath);
            socket = new Socket(ip, Server.SOCKET_PORT);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            Holder holder = new Holder();
            holder.setFileName(srcFile.getName());
            if (sendDone) {
                out.println("join->" + Holder.marshal(holder) + "@");    
            } else {
                out.println("clear->" + Holder.marshal(holder) + "@");
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String msg = in.readLine();
            if ("success".equals(msg) && sendDone) {
                app.logMessage("Sending file " + srcFile.getName() + " done!");
                if (dialog != null) dialog.logMessage("Sending file " + srcFile.getName() + " done!");
            }
        } catch (Exception e) {
            app.logMessage(e.toString());
            if (dialog != null) dialog.logMessage(e.toString());
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (Exception e) {
                    app.logMessage(e.toString());
                }
            }
        }
    }
    
}