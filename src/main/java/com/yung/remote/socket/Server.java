package com.yung.remote.socket;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JOptionPane;

import com.yung.remote.App;
import com.yung.remote.FileUtil;
import com.yung.remote.MarshalHelper;
import com.yung.remote.MessageLog;
import com.yung.remote.MyJDialog;
import com.yung.remote.MySecurityManager;

public class Server {

    public static final int SOCKET_PORT = 17890;
    
    private static Map<String, ArrayList<String>> parts = new HashMap<String, ArrayList<String>>();
    private static int PIPELINE_LIMIT = 2;
    
    public Server(App app) {
        ServerSocket listener = null;
        try {
            listener = new ServerSocket(SOCKET_PORT);
            while (true) {
                new RemoteConnection(listener.accept(), app).start();
            }
        } catch (Exception e) {
            app.logMessage(e.toString());
        } finally {
            try {
                listener.close();
            } catch (IOException e) {
                app.logMessage(e.toString());
            }
        }
    }
    
    public static synchronized boolean setParts(String filename, int idx, String baseText) {
        if (idx < 0) {
            parts.remove(filename);
            return true;
        }
        if (parts.size() > PIPELINE_LIMIT) {
            return false;
        }
        ArrayList<String> list = parts.get(filename);
        if (list == null) {
            list = new ArrayList<String>();
        }
        if (idx >= parts.size()) {
            list.add(baseText);
        } else {
            list.add(idx, baseText);
        }
        parts.put(filename, list);
        return true;
    }
    
    private static class RemoteConnection extends Thread {
        
        private Socket socket;
        private App app;
        
        public RemoteConnection(Socket socket, App app) {
            this.socket = socket;
            this.app = app;
        }
        
        public void run() {
            try {
                final String clientIp = socket.getRemoteSocketAddress().toString();
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                StringBuffer buf = new StringBuffer();
                while (true) {
                    String input = in.readLine();
                    if (input == null || "".equals(input.trim())) {
                        break;
                    }
                    buf.append(input);
                    break;
                }
                String content = buf.toString();
                if (content.startsWith("code->")) {
                    String code = content.substring("code->".length());
                    remote(clientIp, code);
                    ExecutorService executorService = Executors.newFixedThreadPool(1);
                    executorService.execute(new Runnable() {
                        public void run() {
                            try {
                                Thread.sleep(10000);
                                Client client = new Client(app, null);
                                int count = 0;
                                while (count < 500) {
                                    if (!client.checkConnect(clientIp)) {
                                        app.breakConnection();
                                        break;
                                    }
                                    Thread.sleep(5000);
                                    count++;
                                }
                            } catch (Exception e) {
                                app.breakConnection();
                            }
                        }
                    });
                    executorService.shutdown();
                } else if (content.startsWith("msg->")) {
                    String msg = content.substring("msg->".length());
                    showMsg(clientIp, msg);
                } else if (content.startsWith("alert->")) {
                    alert(clientIp, null);
                } else if (content.startsWith("connect->")) {
                    connect(clientIp);
                } else if (content.startsWith("file->")) {
                    int idx = content.indexOf("@");
                    String holderText = content.substring("file->".length(), idx);
                    Holder holder = Holder.unmarshal(holderText);
                    String base64Text = content.substring(idx + 1);
                    getFile(holder.getFileName(), holder.getPart(), holder.getTotal(), base64Text);
                } else if (content.startsWith("join->")) {
                    int idx = content.indexOf("@");
                    String holderText = content.substring("join->".length(), idx);
                    Holder holder = Holder.unmarshal(holderText);
                    joinFile(holder.getFileName());
                } else if (content.startsWith("clear->")) {
                    int idx = content.indexOf("@");
                    String holderText = content.substring("join->".length(), idx);
                    Holder holder = Holder.unmarshal(holderText);
                    clearFile(holder.getFileName());
                }
            } catch (Exception e) {
                app.logMessage(e.toString());
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    app.logMessage(e.toString());
                }
            }
        }

        private void alert(final String clientIp, final String message) {
            ExecutorService executorService = Executors.newFixedThreadPool(1);
            executorService.execute(new Runnable() {
                public void run() {
                    if (message != null) {
                        JOptionPane.showMessageDialog(app.getFrame(), clientIp + ": " + message);
                    } else {
                        JOptionPane.showMessageDialog(app.getFrame(), clientIp + " is calling!");
                    }
                }
            });
            executorService.shutdown();
        }

        private void showMsg(String clientIp, String base64Msg) {
            try {
                String msg = MySecurityManager.decodeBase64(base64Msg);
                String name = clientIp;
                String ip = clientIp;
                int idx = ip.indexOf(":");
                if (idx > 0) {
                    int start = 0;
                    if (ip.startsWith("/")) {
                        start = 1;
                    }
                    ip = ip.substring(start, idx).trim();
                    if (app.getIpMap().containsKey(ip)) {
                        name = app.getIpMap().get(ip);
                    }
                }
                String homeDir = System.getProperty("user.home");
                File logFile = FileUtil.getFile(homeDir + "/.msg/" + ip + ".txt");
                MarshalHelper hepler = new MarshalHelper();
                MessageLog log = null;
                if (logFile.exists()) {
                    log = hepler.unmarshal(logFile, MessageLog.class);
                } else {
                    log = new MessageLog();
                }
                Map<String, MyJDialog> dialogMap = app.getDislogMap();
                MyJDialog dialog = null;
                if (dialogMap.get(name) == null) {
                    dialog = new MyJDialog(app, name, ip, dialogMap.size(), log);
                    // set previous ten logs
                    String msgLogs = log.getLastLogs();
                    dialog.logMessage(msgLogs);
                    // set the size of the window
                    dialog.setSize(MyJDialog.DEFAULT_WIDTH, MyJDialog.DEFAULT_HEIGHT);
                    dialogMap.put(name, dialog);
                } else {
                    dialog = dialogMap.get(name);
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd HH:mm:ss");
                String time = sdf.format(new Date());
                dialog.logMessage(name + "[Time " + time + "]: \n" + msg);
                dialog.addMsgLog(name + "[Time " + time + "]: \n" + msg);
                app.showDialog(dialog);
                app.setMessage(true);
            } catch (Exception e) {
                app.logMessage(e.toString());
            } 
        }

        private void remote(String clientIp, String code) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, IOException {
            Object[] options = new Object[]{"Yes", "No"};
            int n = JOptionPane.showOptionDialog(app.getFrame(), 
                    "Would you like to allow " + clientIp + " to control your machine ?", 
                    "Remote control request", 
                    JOptionPane.YES_NO_OPTION, 
                    JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            if (n == JOptionPane.YES_OPTION) {
                if (app.checkZeroremoteProcess()) {
                    app.breakConnection();
                }
                int idx = clientIp.indexOf(":");
                if (idx > 0) {
                    clientIp = clientIp.substring(1, idx);
                }
                
                MySecurityManager securityMgr = new MySecurityManager();
                String message = securityMgr.decrypt(code);
                if (message.startsWith(clientIp)) {
                    String pass = message.substring(clientIp.length());
                    app.getPwdField().setText(pass);
                    app.setPass();
                    out.println("accept");
                } else {
                    out.println("reject");
                }
            }
        }
        
        private void connect(String clientIp) throws IOException {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            if (app.checkZeroremoteProcess()) {
                out.println("true");
            } else {
                out.println("false");
            }
        }
        
        private void getFile(String fileName, int part, int total, String base64Text) throws IOException {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            if (parts.get(fileName) == null && parts.size() <= PIPELINE_LIMIT) {
                Object[] options = new Object[]{"Yes", "No"};
                int n = JOptionPane.showOptionDialog(app.getFrame(), 
                        "Would you like to receive file " + fileName + " ?", 
                        "Send file request", 
                        JOptionPane.YES_NO_OPTION, 
                        JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
                if (n != JOptionPane.YES_OPTION) {
                    out.print("fail");
                    return;
                }
            }
            if (setParts(fileName, part, base64Text)){
                app.logMessage("transfering file " + fileName + " " + ((part + 1) * 100 / total) + "% ...");
                out.print("success");
            } else {
                setParts(fileName, -1, null);
                out.print("fail");
            }
            out.flush();
        }

        private void joinFile(String fileName) throws IOException {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            if (parts.get(fileName) != null) {
                String downloadDir = System.getProperty("user.home") + "/Downloads";
                File file = FileUtil.getFile(downloadDir + "/" + fileName);
                int count = 1;
                while (count < 10) {
                    if (!file.exists()) {
                        break;
                    }
                    file = FileUtil.getFile(downloadDir + "/" + count + "_" + fileName);
                    count++;
                }
                String base64Text = joinText(fileName);
                MySecurityManager.decompress(base64Text, file);
                app.logMessage("transfer file: " + file.getAbsolutePath() + " done!");
                out.print("success");
            } else {
                out.print("fail");
            }
            out.flush();
            setParts(fileName, -1, null);
        }

        private String joinText(String fileName) {
            ArrayList<String> list = parts.get(fileName);
            StringBuilder sb = new StringBuilder();
            for (String part : list) {
                sb.append(part);
            }
            return sb.toString();
        }
        
        private void clearFile(String fileName) throws IOException {
            setParts(fileName, -1, null);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.print("success");
            out.flush();
        }
        
    }
    
}