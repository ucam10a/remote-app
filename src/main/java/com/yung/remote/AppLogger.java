package com.yung.remote;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AppLogger {

    private static final Level level = Level.INFO;
    
    private static Class<?> debugCls;
    private static String debugMethod;
    private static String mockConfig;
    private static String debugField;
    
    public static Logger getLogger(String cls){
        Logger logger = Logger.getLogger(cls);
        return setLogLevel(logger);
    }
    
    private static Logger setLogLevel(Logger logger){
        logger.setLevel(level);
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(level);
        logger.addHandler(consoleHandler);
        return logger;
    }
    
    public static Class<?> getDebugCls() {
        if (debugCls != null) {
            return debugCls;
        }
        Logger logger = getLogger(AppLogger.class.getName());
        try {
            ClassLoader loader = AppLogger.class.getClassLoader();
            Properties prop = new Properties();
            InputStream stream = loader.getResourceAsStream("debug.properties");
            if (stream == null) return null; 
            prop.load(loader.getResourceAsStream("debug.properties"));
            String debugClsName = prop.getProperty("debugCls");
            if (debugCls == null || debugCls.equals("")) {
                return null;
            }
            debugCls = Class.forName(debugClsName);
            logger.info("debugCls: " + debugCls.getName());
            return debugCls;
        } catch(Exception e) {
            logger.log(Level.FINE, e.toString(), e);
        }
        return null;
    }

    public static String getDebugMethod() {
        if (debugMethod != null) {
            return debugMethod;
        }
        Logger logger = getLogger(AppLogger.class.getName());
        try {
            ClassLoader loader = AppLogger.class.getClassLoader();
            Properties prop = new Properties();
            InputStream stream = loader.getResourceAsStream("debug.properties");
            if (stream == null) return ""; 
            prop.load(loader.getResourceAsStream("debug.properties"));
            String debugMethodName = prop.getProperty("debugMethod");
            if (debugMethodName == null || debugMethodName.equals("")) {
                return "";
            } else {
                debugMethod = debugMethodName;
            }
            logger.info("debugMethod: " + debugMethod);
            return debugMethod;
        } catch(Exception e) {
            logger.log(Level.FINE, e.toString(), e);
        }
        return "";
    }

    public static String getMockConfig() {
        if (mockConfig != null) {
            return mockConfig;
        }
        Logger logger = getLogger(AppLogger.class.getName());
        try {
            ClassLoader loader = AppLogger.class.getClassLoader();
            Properties prop = new Properties();
            InputStream stream = loader.getResourceAsStream("debug.properties");
            if (stream == null) return ""; 
            prop.load(loader.getResourceAsStream("debug.properties"));
            String mockConfigName = prop.getProperty("mockConfig");
            if (mockConfigName == null || mockConfigName.equals("")) {
                return "";
            } else {
                mockConfig = mockConfigName;
            }
            logger.info("mockConfig: " + mockConfig);
            return mockConfig;
        } catch(Exception e) {
            logger.log(Level.FINE, e.toString(), e);
        }
        return "";
    }

    public static String getDebugField() {
        if (debugField != null) {
            return debugField;
        }
        Logger logger = getLogger(AppLogger.class.getName());
        try {
            ClassLoader loader = AppLogger.class.getClassLoader();
            Properties prop = new Properties();
            InputStream stream = loader.getResourceAsStream("debug.properties");
            if (stream == null) return ""; 
            prop.load(loader.getResourceAsStream("debug.properties"));
            String debugFieldName = prop.getProperty("debugField");
            if (debugField == null || debugField.equals("")) {
                return "";
            } else {
                debugField = debugFieldName;
            }
            logger.info("debugField: " + debugField);
            return debugField;
        } catch(Exception e) {
            logger.log(Level.FINE, e.toString(), e);
        }
        return "";
    }
    
}