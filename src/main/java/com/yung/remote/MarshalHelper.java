package com.yung.remote;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Marshal helper to transfer text to object or from object to text
 * 
 * @author Yung Long Li
 *
 */
public class MarshalHelper extends AbstractDebugPrinter {
    
    private Logger logger = AppLogger.getLogger(MarshalHelper.class.getName());
    
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    private boolean compress = false;
    
    private ZipStringTool tool;
    
    public MarshalHelper(String[][] overloadFilters) {
        if (overloadFilters != null) {
            for (String[] arr : overloadFilters) {
                this.addOverloadFilter(arr[0], arr[1]);
            }
        }
    }

    public MarshalHelper() {
    }
    
    public MarshalHelper(ZipStringTool tool) {
        this.compress = true;
        this.tool = tool;
    }

    /**
     * marshal object to string
     * 
     * @param objName object name
     * @param obj object
     * @return marshal string
     */
    public String marshal(String objName, Object obj) {
        try {
            showNull = false;
            showFieldType = true;
            showOutOfBuffer = false;
            RecordBuffer bud = new RecordBuffer(debugCls);
            bud = printObjectParam(bud, "", objName, obj);
            return bud.toString();
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, e.toString(), e);
            throw e;
        }
    }
    
    /**
     * marshal object to file
     * 
     * @param fileFolder file folder
     * @param objName object name
     * @param obj object
     * @throws IOException 
     */
    public void marshal(String fileFolder, String objName, Object obj) throws IOException {
        marshal(fileFolder, objName, objName, obj);
    }
    
    /**
     * marshal object to file
     * 
     * @param fileFolder file folder
     * @param filename file name
     * @param objName object name
     * @param obj object
     * @throws IOException 
     */
    public void marshal(String fileFolder, String filename, String objName, Object obj) throws IOException {
        
        try {
            showNull = false;
            showFieldType = true;
            showOutOfBuffer = false;
            RecordBuffer bud = new RecordBuffer(debugCls);
            bud = printObjectParam(bud, "", objName, obj);
            if (compress) {
                writeTextToFile(fileFolder + "/" + filename + ".txt" , tool.compress(bud.toString()));
            } else {
                writeTextToFile(fileFolder + "/" + filename + ".txt" , bud.toString());
            }
        } catch (RuntimeException e) {
            logger.log(Level.SEVERE, e.toString(), e);
            throw e;
        }
    }
    
    private List<String> readCompressFile(File file) throws IOException {
        List<String> result = new ArrayList<String>();
        StringBuilder bud = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) bud.append(line);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        String txt = tool.decompress(bud.toString());
        Scanner scanner = new Scanner(txt);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line != null) {
                if (!line.trim().equals("") && !line.trim().equals("null")) {
                    result.add(line);
                }
            }
        }
        scanner.close();
        return result;
    }
    
    /**
     * unmarshal text to object
     * 
     * @param txt marshal text
     * @param cls object class
     * @return object
     */
    public <T> T unmarshal(String txt, Class<T> cls) {
        try {
            List<String> lines = new ArrayList<String>();
            Scanner scanner = new Scanner(txt);
            while (scanner.hasNextLine()) {
              String line = scanner.nextLine();
              if (line != null) {
                  if (!line.trim().equals("") && !line.trim().equals("null")) {
                      lines.add(line);
                  }
              }
            }
            scanner.close();
            T result = construct(lines, cls);
            return result;
        } catch (Exception e) {
            if (logger != null) logger.log(Level.SEVERE, e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
    /**
     * unmarshal text to list
     * 
     * @param txt marshal text
     * @param cls object class
     * @return object list
     */
    @SuppressWarnings("rawtypes")
    public <T> List<T> unmarshalList(String txt, Class<T> cls) {
        try {
            List<String> data = new ArrayList<String>();
            Scanner scanner = new Scanner(txt);
            while (scanner.hasNextLine()) {
              String line = scanner.nextLine();
              if (line != null) {
                  line = line.trim();
                  if (!line.equals("") && !line.equals("null")) {
                      data.add(line);
                  }
              }
            }
            scanner.close();
            String type = getCollectionType(data.get(0));
            Object list = Class.forName(type).newInstance();
            List<T> result = constructList(data, cls, (List) list);
            return result;
        } catch (Exception e) {
            if (logger != null) logger.log(Level.SEVERE, e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
    /**
     * unmarshal text to array
     * 
     * @param txt marshal text
     * @param cls object class
     * @return object array
     */
    public <T> T[] unmarshalArray(String txt, Class<T> cls) {
        try {
            List<String> data = new ArrayList<String>();
            Scanner scanner = new Scanner(txt);
            while (scanner.hasNextLine()) {
              String line = scanner.nextLine();
              if (line != null) {
                  line = line.trim();
                  if (!line.equals("") && !line.equals("null")) {
                      data.add(line);
                  }
              }
            }
            scanner.close();
            T[] result = constructArray(data, cls);
            return result;
        } catch (Exception e) {
            if (logger != null) logger.log(Level.SEVERE, e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
    /**
     * unmarshal file to object
     * 
     * @param file file
     * @param cls object class
     * @return object
     */
    public <T> T unmarshal(File file, Class<T> cls) {
        try {
            if (!file.exists()) return null;
            List<String> data = null;
            if (compress) {
                data = readCompressFile(file);
            } else {
                data = readFile(file);
            }
            T result = construct(data, cls);
            return result;
        } catch (Exception e) {
            if (logger != null) logger.log(Level.SEVERE, file.getAbsolutePath() + ", " + e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
    /**
     * unmarshal file to list
     * 
     * @param file file 
     * @param cls object class
     * @return list object
     */
    @SuppressWarnings("rawtypes")
    public <T> List<T> unmarshalList(File file, Class<T> cls) {
        try {
            if (!file.exists()) return null;
            List<String> data = null;
            if (compress) {
                data = readCompressFile(file);
            } else {
                data = readFile(file);
            }
            String type = getCollectionType(data.get(0));
            Object list = Class.forName(type).newInstance();
            List<T> result = constructList(data, cls, (List) list);
            return result;
        } catch (Exception e) {
            if (logger != null) logger.log(Level.SEVERE, file.getAbsolutePath() + ", " + e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
    /**
     * unmarshal file to array
     * 
     * @param file file 
     * @param cls object class
     * @return array object
     */
    public <T> T[] unmarshalArray(File file, Class<T> cls) {
        try {
            if (!file.exists()) return null;
            List<String> data = null;
            if (compress) {
                data = readCompressFile(file);
            } else {
                data = readFile(file);
            }
            T[] result = constructArray(data, cls);
            return result;
        } catch (Exception e) {
            if (logger != null) logger.log(Level.SEVERE, file.getAbsolutePath() + ", " + e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
    /**
     * unmarshal file to map
     * 
     * @param file file
     * @param keyCls key class
     * @param valueCls value class
     * @return map object
     */
    @SuppressWarnings({"unchecked" })
    public <K, V> Map<K, V> unmarshalMap(File file, Class<K> keyCls, Class<V> valueCls) {
        try {
            if (!file.exists()) return null;
            List<String> data = null;
            if (compress) {
                data = readCompressFile(file);
            } else {
                data = readFile(file);
            }
            String type = getCollectionType(data.get(0));
            Object map = Class.forName(type).newInstance();
            Map<K, V> result = constructMap(data, keyCls, valueCls, (Map<K, V>) map);
            return result;
        } catch (Exception e) {
            if (logger != null) logger.log(Level.SEVERE, file.getAbsolutePath() + ", " + e.toString(), e);
            throw new RuntimeException(e);
        }
    }
    
    @SuppressWarnings("rawtypes")
    private <T> T construct(List<String> data, Class<T> cls) throws Exception {
        if (isDebug(cls, "", "")) {
            logger.info("debug class: " + debugCls.getName());
        }
        if (data.size() == 1) {
            if (!isComplexType(data.get(0))) {
                if (data.get(0).trim().startsWith("object name:") && data.get(0).contains(",")) {
                    return cls.newInstance();
                } else {
                    Object result = getFieldObject(cls.getName(), data.get(0));
                    if (cls.isAssignableFrom(result.getClass())) {
                        return cls.cast(result);
                    }
                }
            }
        }
        T result = cls.newInstance();
        if (data == null || data.size() == 0) {
            return null;
        } else {
            int lineIdx = 1;
            while (lineIdx < data.size()) {
                String line = data.get(lineIdx);
                if (isComplexType(line)) {
                    String type = getCollectionType(line);
                    String name = getCollectionObjectName(line);
                    int indentSize = getIndentSize(line);
                    Class<?> collectionTypeCls = Class.forName(type);
                    if ((lineIdx + 1) >= data.size()) {
                        // complex type has no element
                        if (isDebug(null, null, name)) {
                            logger.info("debug field: " + debugField);
                        }
                        runSetter(result, name, collectionTypeCls.newInstance());
                        break;
                    }
                    int lastIdx = getLastNextDataIndex(data, lineIdx + 1, indentSize);
                    List<String> nextData = getNextData(data, lineIdx, lastIdx, indentSize);
                    if (isComplexType(data.get(lineIdx + 1))) {
                        // next line is also a complex type
                        Object obj = construct(nextData, collectionTypeCls);
                        if (obj != null) {
                            if (isDebug(null, null, name)) {
                                logger.info("debug field: " + debugField);
                            }
                            runSetter(result, name, obj);
                        }
                        lineIdx = lastIdx + 1;
                        continue;
                    }
                    if (collectionTypeCls.isArray()) {
                        String componentType = getObjectClsName(data.get(lineIdx + 1));
                        if (componentType == null) {
                            componentType = getFieldType(data.get(lineIdx + 1));
                        }
                        if (componentType == null) {
                            throw new Exception("can not determine component type for line: " + data.get(lineIdx + 1));
                        }
                        Class<?> objCls = Class.forName(componentType);
                        Object array = constructArray(nextData, objCls);
                        if (array != null) {
                            if (isDebug(null, null, name)) {
                                logger.info("debug field: " + debugField);
                            }
                            array = convertToPrimitiveArray(getFieldComponentType(result.getClass(), name), array);
                            runSetter(result, name, array);
                        }
                        lineIdx = lastIdx + 1;
                    } else {
                        Object complexType = null;
                        if (collectionTypeCls.getName().equals("java.util.Arrays$ArrayList")) {
                            complexType = new ArrayList();;
                        } else {
                            complexType = collectionTypeCls.newInstance();
                        }
                        if (complexType instanceof List) {
                            String componentType = getObjectClsName(data.get(lineIdx + 1));
                            if (componentType == null) {
                                componentType = getFieldType(data.get(lineIdx + 1));
                            }
                            if (componentType == null) {
                                throw new Exception("can not determine component type for line: " + data.get(lineIdx + 1).trim());
                            }
                            Class<?> objCls = Class.forName(componentType);
                            List list = constructList(nextData, objCls, (List) complexType);
                            if (list != null) {
                                if (isDebug(null, null, name)) {
                                    logger.info("debug field: " + debugField);
                                }
                                runSetter(result, name, list);
                            }
                            lineIdx = lastIdx + 1;
                        } else if (complexType instanceof Set) {
                            String componentType = getObjectClsName(data.get(lineIdx + 1));
                            if (componentType == null) {
                                componentType = getFieldType(data.get(lineIdx + 1));
                            }
                            if (componentType == null) {
                                throw new Exception("can not determine component type for line: " + data.get(lineIdx + 1));
                            }
                            Class<?> objCls = Class.forName(componentType);
                            Set set = constructSet(nextData, objCls, (Set) complexType);
                            if (set != null) {
                                if (isDebug(null, null, name)) {
                                    logger.info("debug field: " + debugField);
                                }
                                runSetter(result, name, set);
                            }
                            lineIdx = lastIdx + 1;
                        } else if (complexType instanceof Map) {
                            String keyType = getKeyType(nextData);
                            String valueType = getValueType(nextData);
                            Class<?> keyCls = Class.forName(keyType);
                            Class<?> valueCls = Class.forName(valueType);
                            @SuppressWarnings("unchecked")
                            Map<?, ?> map = constructMap(nextData, keyCls, valueCls, (Map) complexType);
                            if (map != null) {
                                if (isDebug(null, null, name)) {
                                    logger.info("debug field: " + debugField);
                                }
                                runSetter(result, name, map);
                            }
                            lineIdx = lastIdx + 1;
                        }
                    }
                } else {
                    if (line.trim().startsWith("object name:") && line.contains(",")) {
                        String objClsName = getObjectClsName(line);
                        String fieldName = getObjectFieldName(line);
                        Class<?> objCls = Class.forName(objClsName);
                        int indentSize = getIndentSize(line);
                        // get field data
                        lineIdx++;
                        int lastIdx = getLastNextDataIndex(data, lineIdx, indentSize);
                        List<String> nextData = getNextData(data, lineIdx - 1, lastIdx, indentSize);
                        Object obj = construct(nextData, objCls);
                        if (obj != null) {
                            if (isDebug(null, null, fieldName)) {
                                logger.info("debug field: " + debugField);
                            }
                            runSetter(result, fieldName, obj);
                        }
                        lineIdx = lastIdx + 1;
                    } else {
                        String type = getFieldType(line);
                        String fieldName = getFieldName(line);
                        Object obj = getFieldObject(type, line);
                        if (obj != null) {
                            if (isDebug(null, null, fieldName)) {
                                logger.info("debug field: " + debugField);
                            }
                            runSetter(result, fieldName, obj);
                        }
                        lineIdx++;
                    }
                }
            }
        }
        return result;
    }
    
    private List<String> removeFirstLine(List<String> nextData, int indentSize) {
        List<String> result = new ArrayList<String>();
        for (int i = 1; i < nextData.size(); i++) {
            result.add(nextData.get(i).substring(indentSize));
        }
        return result;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private <T> List<T> constructList(List<String> data, Class<T> cls, List list) throws Exception {
        if (isDebug(cls, "", "")) {
            logger.info("debug class: " + debugCls.getName());
        }
        if (data.size() > 0) data = removeFirstLine(data, 4);
        int index = 0;
        while (index < data.size()) {
            int lastIdx = getLastNextDataIndex(data, index, 0);
            List<String> nextData = getNextData(data, index, lastIdx, 0);
            T element = null;
            if (nextData.size() != 0 && nextData.size() == 1) {
                element = (T) getFieldObject(cls.getName() , nextData.get(0));
            } else {
                element = construct(nextData, cls);
            }
            list.add(element);
            index = lastIdx + 1;
        }
        return list;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private <T> Set<T> constructSet(List<String> data, Class<T> cls, Set set) throws Exception {
        if (isDebug(cls, "", "")) {
            logger.info("debug class: " + debugCls.getName());
        }
        if (data.size() > 0) data = removeFirstLine(data, 4);
        int index = 0;
        while (index < data.size()) {
            int lastIdx = getLastNextDataIndex(data, index, 0);
            List<String> nextData = getNextData(data, index, lastIdx, 0);
            T element = null;
            if (nextData.size() != 0 && nextData.size() == 1) {
                element = (T) getFieldObject(cls.getName() , nextData.get(0));
            } else {
                element = construct(nextData, cls);
            }
            set.add(element);
            index = lastIdx + 1;
        }
        return set;
    }
    
    @SuppressWarnings("unchecked")
    private <T> T[] constructArray(List<String> data, Class<T> cls) throws Exception {
        if (isDebug(cls, "", "")) {
            logger.info("debug class: " + debugCls.getName());
        }
        if (data.size() > 0) data = removeFirstLine(data, 4);
        List<T> list = new ArrayList<T>();
        int index = 0;
        while (index < data.size()) {
            int lastIdx = getLastNextDataIndex(data, index, 0);
            List<String> nextData = getNextData(data, index, lastIdx, 0);
            T element = null;
            if (nextData.size() != 0 && nextData.size() == 1) {
                element = (T) getFieldObject(cls.getName() , nextData.get(0));
            } else {
                element = construct(nextData, cls);
            }
            list.add(element);
            index = lastIdx + 1;
        }
        T[] array = (T[]) Array.newInstance(cls, list.size());
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }
    
    private String getValueType(List<String> data) throws Exception {
        String line = "";
        for (String str : data){
            if (str.trim().contains("value")) {
                line = str;
                break;
            }
        }
        String componentType = getObjectClsName(line);
        if (componentType == null) {
            componentType = getFieldType(line);
        }
        if (componentType == null) {
            throw new Exception("can not determine component type for line: " + line);
        }
        return componentType;
    }

    private String getKeyType(List<String> data) throws Exception {
        String line = "";
        for (String str : data){
            if (str.trim().contains("key")) {
                line = str;
                break;
            }
        }
        String componentType = getObjectClsName(line);
        if (componentType == null) {
            componentType = getFieldType(line);
        }
        if (componentType == null) {
            throw new Exception("can not determine component type for line: " + line);
        }
        return componentType;
    }

    @SuppressWarnings("unchecked")
    private <K, V> Map<K, V> constructMap(List<String> data, Class<K> keyCls, Class<V> valueCls, Map<K, V> map) throws Exception {
        if (isDebug(keyCls, "", "") || isDebug(valueCls, "", "")) {
            logger.info("debug class: " + debugCls.getName());
        }
        if (data.size() > 0) data = removeFirstLine(data, 4);
        int index = 0;
        int lastIdx = 0;
        List<String> nextData;
        while (index < data.size()) {
            // get key
            lastIdx = getLastNextDataIndex(data, index, 0);
            nextData = getNextData(data, index, lastIdx, 0);
            K key = null;
            if (nextData.size() != 0 && nextData.size() == 1) {
                String keyType = getFieldType(nextData.get(0));
                key = (K) getFieldObject(keyType , nextData.get(0));
            } else {
                key = construct(nextData, keyCls);
            }
            index = lastIdx + 1;
            // get value
            lastIdx = getLastNextDataIndex(data, index, 0);
            nextData = getNextData(data, index, lastIdx, 0);
            V value = null;
            if (nextData.size() != 0 && nextData.get(0).startsWith("value")) {
                if (nextData.size() != 0 && nextData.size() == 1) {
                    String valueType = getFieldType(nextData.get(0));
                    value = (V) getFieldObject(valueType , nextData.get(0));
                } else {
                    value = construct(nextData, valueCls);
                }
            } else {
                lastIdx = index - 1;
            }
            index = lastIdx + 1;
            map.put(key, value);
        }
        return map;
    }

    private String getCollectionObjectName(String line) throws Exception {
        int index = line.indexOf(":");
        if (index != -1) {
            return line.substring(index + 1).trim();
        }
        throw new Exception("Can not parse name for line: " + line);
    }

    private String getCollectionType(String line) throws Exception {
        int index = line.indexOf(":");
        if (index != -1) {
            return line.substring(0, index).trim();
        }
        throw new Exception("Can not parse type for line: " + line);
    }

    private String getFieldName(String line) throws Exception {
        int index = line.indexOf("[");
        if (index == -1) {
            throw new Exception("no field name for: " + line);
        }
        return line.substring(0, index).trim();
    }

    private String getFieldType(String line) throws Exception {
        int start = line.indexOf("[");
        int end = line.indexOf("]");
        if (start == -1 || end == -1) {
            throw new Exception("no field type for: " + line);
        }
        return line.substring(start + 1, end).trim();
    }

    private Object getFieldObject(String type, String line) throws ClassNotFoundException, ParseException {
        int index = line.indexOf("=") + 1;
        String content = line.substring(index).trim();
        Class<?> cls = Class.forName(type);
        if (cls == String.class) {
            return new String(content);
        } else if (cls == Integer.class) {
            return new Integer(content);
        } else if (cls == Double.class) {
            return new Double(content);
        } else if (cls == Float.class) {
            return new Float(content);
        } else if (cls == Long.class) {
            return new Long(content);
        } else if (cls == Short.class) {
            return new Short(content);
        } else if (cls == BigInteger.class) {
            return new BigInteger(content);
        } else if (cls == BigDecimal.class) {
            return new BigDecimal(content);
        } else if (cls == Boolean.class) {
            return new Boolean(content);
        } else if (cls == Date.class) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            return sdf.parse(content);
        } else if (cls == Timestamp.class) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date dt = sdf.parse(content);
            return new Timestamp(dt.getTime());
        } else if (cls == java.sql.Date.class) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date dt = sdf.parse(content);
            return new java.sql.Date(dt.getTime());
        }
        return null;
    }

    private boolean isComplexType(String line) throws Exception {
        line = line.trim();
        if (!line.startsWith("object name:")){
            int index = line.indexOf(":");
            if (index != -1 && !line.contains("]")) {
                String type = line.substring(0, index).trim();
                Class<?> cls = Class.forName(type);
                if (cls.isArray()){
                    return true;
                }
                if (cls.getName().equals("java.util.Arrays$ArrayList")) {
                    return true;
                }
                Object obj = cls.newInstance();
                if (obj instanceof List) {
                    return true;
                } else if (obj instanceof Set) {
                    return true;
                } else if (obj instanceof Map) {
                    return true;
                }
            }
        }
        return false;
    }

    private String getObjectFieldName(String line) {
        int start = line.indexOf(":") + 1;
        int end = line.indexOf(",");
        return line.substring(start, end).trim();
    }

    private String getObjectClsName(String line) {
        line = line.trim();
        if (line.trim().startsWith("object name:")) {
            int index = line.indexOf(",");
            return line.substring(index + 1).trim();
        } else {
            return null;
        }
    }

    private List<String> getNextData(List<String> data, int lineIdx, int lastDataIndex, int indentSize) {
        int index = lineIdx;
        List<String> result = new ArrayList<String>();
        while (index <= lastDataIndex && index < data.size()) {
            String line = data.get(index);
            line = line.substring(indentSize);
            result.add(line);
            index++;
        }
        return result;
    }

    private int getLastNextDataIndex(List<String> data, int lineIdx, int indentSize) {
        int index = lineIdx + 1;
        while (index < data.size()) {
            String line = data.get(index);
            if (line.charAt(indentSize) != ' ') {
                return index - 1;
            }
            index++;
        }
        return index - 1;
    }

    private int getIndentSize(String line) {
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == ' ') {
                continue;
            } else {
                return i;
            }
        }
        return -1;
    }
    
    private List<String> readFile(File file) throws IOException {
        List<String> result = new ArrayList<String>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) result.add(line);
                line = br.readLine();
            }
            return result;
        } finally {
            br.close();
        }
    }
    
    private File writeTextToFile(String file, String inputText) {
        BufferedWriter writer = null;
        File logFile = null;
        try {
            logFile = new File(file);
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(inputText);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.toString(), e);
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
        return logFile;
    }
    
    @Override
    public void printMessage(String message) {
        logger.fine("printMessage ignore");
    }
    
    public Class<?> getReturnObjectType(File file) throws Exception {
        List<String> data = readFile(file);
        if (data.size() > 0) {
            String line = data.get(0);
            if (line.startsWith("object name:")) {
                String type = getObjectClsName(line);
                return Class.forName(type);
            } else {
                if (data.size() == 1) {
                    String type = getFieldType(line);
                    return Class.forName(type);
                } else {
                    throw new Exception("file: " + file.getAbsolutePath() + " is not an object");    
                }
            }
        } else {
            throw new Exception("file: " + file.getAbsolutePath() + " is empty");
        }
    }

    public Class<?> getReturnArrayType(File file) throws Exception {
        List<String> data = readFile(file);
        if (data.size() > 1) {
            String line = data.get(0);
            if (isComplexType(line)) {
                String componentType = getObjectClsName(data.get(1));
                if (componentType == null) {
                    componentType = getFieldType(data.get(1));
                }
                if (componentType == null) {
                    throw new Exception("can not determine component type for line: " + data.get(1));
                }
                return Class.forName(componentType);
            } else {
                throw new Exception("file: " + file.getAbsolutePath() + " is not an array");
            }
        } else {
            if (data.size() == 1) {
                return null;
            }
            throw new Exception("file: " + file.getAbsolutePath() + " is empty");
        }
    }

    public Class<?> getReturnListType(File file) throws Exception {
        List<String> data = readFile(file);
        if (data.size() > 1) {
            String line = data.get(0);
            if (isComplexType(line)) {
                String componentType = getObjectClsName(data.get(1));
                if (componentType == null) {
                    componentType = getFieldType(data.get(1));
                }
                if (componentType == null) {
                    throw new Exception("can not determine component type for line: " + data.get(1));
                }
                return Class.forName(componentType);
            } else {
                throw new Exception("file: " + file.getAbsolutePath() + " is not a list");
            }
        } else {
            if (data.size() == 1) {
                return null;
            }
            throw new Exception("file: " + file.getAbsolutePath() + " is empty");
        }
    }

    public Class<?> getReturnMapValueType(File file) throws Exception {
        List<String> data = readFile(file);
        if (data.size() > 1) {
            String line = data.get(0);
            if (isComplexType(line)) {
                int indentSize = getIndentSize(line);
                int lastIdx = getLastNextDataIndex(data, 1, indentSize);
                List<String> nextData = getNextData(data, 0, lastIdx, indentSize);
                String valueType = getValueType(nextData);
                return Class.forName(valueType);
            } else {
                throw new Exception("file: " + file.getAbsolutePath() + " is not a map");
            }
        } else {
            if (data.size() == 1) {
                return null;
            }
            throw new Exception("file: " + file.getAbsolutePath() + " is empty");
        }
    }

    public Class<?> getReturnMapKeyType(File file) throws Exception {
        List<String> data = readFile(file);
        if (data.size() > 1) {
            String line = data.get(0);
            if (isComplexType(line)) {
                int indentSize = getIndentSize(line);
                int lastIdx = getLastNextDataIndex(data, 1, indentSize);
                List<String> nextData = getNextData(data, 0, lastIdx, indentSize);
                String keyType = getKeyType(nextData);
                return Class.forName(keyType);
            } else {
                throw new Exception("file: " + file.getAbsolutePath() + " is not a map");
            }
        } else {
            if (data.size() == 1) {
                return null;
            }
            throw new Exception("file: " + file.getAbsolutePath() + " is empty");
        }
    }
    
    private Object convertToPrimitiveArray(Class<?> type, Object serviceResult) {
        Object[] val = null;
        if (serviceResult instanceof Object[]) {
            val = (Object[]) serviceResult;
            if (type == int.class) {
                int[] result = new int[val.length];
                for (int i = 0; i < val.length; i++) {
                    result[i] = (Integer) val[i];
                }
                return result;
            } else if (type == double.class) {
                double[] result = new double[val.length];
                for (int i = 0; i < val.length; i++) {
                    result[i] = (Double) val[i];
                }
                return result;
            } else if (type == float.class) {
                float[] result = new float[val.length];
                for (int i = 0; i < val.length; i++) {
                    result[i] = (Float) val[i];
                }
                return result;
            } else if (type == long.class) {
                long[] result = new long[val.length];
                for (int i = 0; i < val.length; i++) {
                    result[i] = (Long) val[i];
                }
                return result;
            } else {
                throw new RuntimeException("not support " + type.getName());
            }
        } else {
            throw new RuntimeException(serviceResult.getClass().getName() + " is not Object array");
        }
    }
    
    private Class<?> getFieldComponentType(Class<?> cls, String name) throws NoSuchFieldException, SecurityException {
        Field field = cls.getDeclaredField(name);
        if (field.getType().isArray()) {
            return field.getType().getComponentType();
        } else {
            throw new RuntimeException("field type: " + field.getType() + " is not array");
        }
    }
    
    
    
}