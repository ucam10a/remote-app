package com.yung.remote;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

public class MySecurityManager {

    private static final String KEY = "3WpYNCudcfEGpnqt6dOlEQ==";
    private Cipher c;
    private SecretKeySpec spec;
    private byte[] cipherByte;
    
    public static String generateKey() throws NoSuchAlgorithmException {
        KeyGenerator keygen = KeyGenerator.getInstance("AES");
        keygen.init(128);
        SecretKey deskey = keygen.generateKey();
        byte[] key = deskey.getEncoded();
        return new String(Base64.encodeBase64String(key));
    }
    
    public MySecurityManager() throws NoSuchAlgorithmException, NoSuchPaddingException {
        spec = new SecretKeySpec(Base64.decodeBase64(KEY), "AES");
        c = Cipher.getInstance("AES");
    }
    
    public MySecurityManager(String key) throws NoSuchAlgorithmException, NoSuchPaddingException {
        spec = new SecretKeySpec(Base64.decodeBase64(key), "AES");
        c = Cipher.getInstance("AES");
    }
    
    public byte[] encrptor(String str) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        c.init(Cipher.ENCRYPT_MODE, spec);
        byte[] src = str.getBytes();
        cipherByte = c.doFinal(src);
        return cipherByte;
    }
    
    public String encrypt(String input) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        byte[] arr = encrptor(input);
        return new String(Base64.encodeBase64String(arr));
    }
    
    public byte[] decrptor(byte[] buff) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        c.init(Cipher.DECRYPT_MODE, spec);
        cipherByte = c.doFinal(buff);
        return cipherByte;
    }
    
    public String decrypt(String code) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        byte[] arr = Base64.decodeBase64(code);
        return new String(decrptor(arr));
    }
    
    public static String encodeBase64(String originalText) throws IOException {
        return compress(originalText);
    }
    
    public static String compress(String srcTxt) throws IOException {
        ByteArrayOutputStream rstBao = new ByteArrayOutputStream();
        GZIPOutputStream zos = new GZIPOutputStream(rstBao);
        zos.write(srcTxt.getBytes("UTF-8"));
        IOUtils.closeQuietly(zos);
        byte[] bytes = rstBao.toByteArray();
        return Base64.encodeBase64String(bytes);
    }
    
    public static String decompress(String zippedBase64Str) throws IOException {
        String result = null;
        byte[] bytes = Base64.decodeBase64(zippedBase64Str);
        GZIPInputStream zi = null;
        try {
            zi = new GZIPInputStream(new ByteArrayInputStream(bytes));
            StringWriter writer = new StringWriter();
            IOUtils.copy(zi, writer, "UTF-8");
            result = writer.toString();
        } finally {
            IOUtils.closeQuietly(zi);
        }
        return result;
    }
    
    public static String decodeBase64(String base64) throws IOException {
        if (base64.equals("")) return "";
        return decompress(base64);
    }

    public static void decompress(String base64Text, File out) throws IOException {
        FileOutputStream f = new FileOutputStream(out);
        byte[] arr = Base64.decodeBase64(base64Text);
        f.write(arr);
        f.close();
    }

    public static String compress(File srcFile) throws IOException {
        byte[] array = Files.readAllBytes(srcFile.toPath());
        return Base64.encodeBase64String(array);
    }
    
    public static void main(String args[]) throws Exception {
        
        String key = generateKey();
        System.out.println(key);
        
    }
    
}