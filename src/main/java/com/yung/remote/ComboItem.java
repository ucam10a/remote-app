package com.yung.remote;

public class ComboItem {

    private String content = "";

    public ComboItem(String content) {
        this.content = content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    @Override
    public String toString() {
        return content;
    }
    
}
