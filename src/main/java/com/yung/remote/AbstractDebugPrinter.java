package com.yung.remote;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

/**
 * For debug only
 * 
 * @author Yung Long Li
 * 
 */
public abstract class AbstractDebugPrinter extends PlainObjectOperator {
    
    private static final Logger logger = AppLogger.getLogger(AbstractDebugPrinter.class.getName());
    private static final int MAX_INDENT = 40;
    protected boolean showOutOfBuffer = true; 
    protected boolean showNull = true;
    protected boolean showFieldType = false;
    protected boolean encloseString = false;
    protected List<String> allowType = new ArrayList<String>();
    protected List<String> filterType = new ArrayList<String>();
    private Map<String, List<String>> fieldFilter = new HashMap<String, List<String>>();
    public Class<?> debugCls = AppLogger.getDebugCls();
    public String debugMethod = AppLogger.getDebugMethod();
    public String debugField = AppLogger.getDebugField();
    
    private static final String MARKER = "****";
    
    public static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    public AbstractDebugPrinter(){
        filterType.add("java.lang.Class");
        filterType.add("com.sun.proxy");
        filterType.add("java.lang.reflect");
    }
    
    /**
     * log message
     * 
     * @param message message
     */
    public abstract void printMessage(String message);

    protected RecordBuffer printMarkMessage(RecordBuffer bud, String indent, String message, int hashcode) {
        if (indent == null || indent.equals("")) {
            bud.append(message + LINE_SEPARATOR, hashcode);
        } else {
            bud.append(indent + message + LINE_SEPARATOR, hashcode);
        }
        return bud;
    }

    protected RecordBuffer printList(RecordBuffer bud, String indent, String objName, List<?> list) {
        try {
            if (list.size() > 0 && isDebug(list.get(0).getClass(), "", "")) {
                logger.info("printList: " + objName);
            }
            int i = 1;
            bud = printMarkMessage(bud, indent, list.getClass().getName() + ": " + objName, list.getClass().getName().hashCode());
            for (Object obj : list) {
                bud = printObjectParam(bud, indent + "    ", objName + i, obj);
                i++;
            }
            return bud;
        } catch (Exception e) {
            logger.severe("printList error objName: " + objName);
            if (list != null && list.size() > 0) {
                logger.severe("printList error obj class: " + list.get(0).getClass().getName());
            }
            throw new RuntimeException(e);
        }
    }

    protected RecordBuffer printSet(RecordBuffer bud, String indent, String objName, Set<?> set) {
        try {
            if (set.size() > 0 && isDebug(set.iterator().next().getClass(), "", "")) {
                logger.info("printSet: " + objName);
            }
            int i = 1;
            bud = printMarkMessage(bud, indent, set.getClass().getName() + ": " + objName, set.getClass().getName().hashCode());
            for (Object obj : set) {
                bud = printObjectParam(bud, indent + "    ", objName + i, obj);
                i++;
            }
            return bud;
        } catch (Exception e) {
            logger.severe("printSet error objName: " + objName);
            if (set != null && set.size() > 0) {
                logger.severe("printSet error obj class: " + set.iterator().next().getClass().getName());
            }
            throw new RuntimeException(e);
        }
    }

    protected RecordBuffer printMap(RecordBuffer bud, String indent, String objName, Map<?, ?> map) {
        Class<?> keyCls = getKeyCls(map);
        Class<?> valueCls = getValueCls(map);
        try {
            if (map.size() > 0 && (isDebug(keyCls, "", "") || isDebug(valueCls, "", ""))) {
                logger.info("printMap: " + objName);
            }
            int i = 1;
            bud = printMarkMessage(bud, indent, map.getClass().getName() + ": " + objName, map.getClass().getName().hashCode());
            for (Entry<?, ?> entry : map.entrySet()) {
                bud = printObjectParam(bud, indent + "    ", "key" + i, entry.getKey());
                bud = printObjectParam(bud, indent + "    ", "value" + i, entry.getValue());
                i++;
            }
            return bud;
        } catch (Exception e) {
            logger.severe("printMap error objName: " + objName);
            logger.severe("printMap error key class: " + keyCls + ", value class: " + valueCls);
            throw new RuntimeException(e);
        }
    }

    private Class<?> getValueCls(Map<?, ?> map) {
        if (map.size() > 0) {
            for (Object key : map.keySet()) {
                if (key != null) return key.getClass();
            }
        }
        return null;
    }

    private Class<?> getKeyCls(Map<?, ?> map) {
        if (map.size() > 0) {
            for (Object value : map.values()) {
                if (value != null) return value.getClass();
            }
        }
        return null;
    }

    private RecordBuffer printArray(RecordBuffer bud, String indent, String objName, Object array) {
        try {
            if (Array.getLength(array) > 0 && isDebug(array.getClass().getComponentType(), "", "")) {
                logger.info("printArray: " + objName);
            }
            bud = printMarkMessage(bud, indent, Object[].class.getName() + ": " + objName, array.getClass().getName().hashCode());
            for (int i = 0; i < Array.getLength(array); i++) {
                bud = printObjectParam(bud, indent + "    ", objName + i, Array.get(array, i));
            }
            return bud;
        } catch (Exception e) {
            logger.severe("printArray error objName: " + objName);
            if (array != null) {
                logger.severe("printArray error obj class: " + array.getClass().getComponentType().getName());
            }
            throw new RuntimeException(e);
        }
    }

    /**
     * print object
     * 
     * @param bud String buffer
     * @param indent indent string, 4 spaces
     * @param objName object name
     * @param obj object
     * @return output string
     */
    protected RecordBuffer printObjectParam(RecordBuffer bud, String indent, String objName, Object obj) {
        try {
            if (indent.length() > MAX_INDENT) {
                if (showOutOfBuffer) bud.append(indent + "skip following objet ..." + LINE_SEPARATOR, 1);
                return bud;
            }
            if (obj == null) {
                if (showNull) bud = printMarkMessage(bud, indent, "Object:" + objName + " is null", 1);
                return bud;
            }
            if (isFilter(obj.getClass().getName())) {
                return bud;
            }
            if (isDebug(obj.getClass(), "", "")) {
                logger.info("printObjectParam: " + objName);
            }
            if (obj instanceof List) {
                List<?> list = (List<?>) obj;
                bud = printList(bud, indent, objName, list);
            } else if (obj.getClass().isArray()) {
                bud = printArray(bud, indent, objName, obj);
            } else if (obj instanceof Set) {
                Set<?> set = (Set<?>) obj;
                bud = printSet(bud, indent, objName, set);
            } else if (obj instanceof Map) {
                Map<?, ?> map = (Map<?, ?>) obj;
                bud = printMap(bud, indent, objName, map);
            } else {
                if (allowType.size() != 0 && !allowType.contains(obj.getClass().getName())) {
                    return bud;
                }
                if (allowType(obj.getClass())) {
                    String value = getObjectString(obj);
                    if (showFieldType) {
                        bud.append(indent + objName + "[" + obj.getClass().getName() + "] = " + value + LINE_SEPARATOR, value.hashCode());
                    } else {
                        bud.append(indent + objName + " = " + value + LINE_SEPARATOR, value.hashCode());
                    }
                } else {
                    Class<?> cls = obj.getClass();
                    bud = printMarkMessage(bud, indent, "object name:" + objName + ", " + cls.getName(), cls.getName().hashCode());
                    // print field
                    boolean start = true;
                    while (cls != null) {
                        Field[] fields = cls.getDeclaredFields();
                        if (start) {
                            indent = "    " + indent;
                            start = false;
                        }
                        for (Field f : fields) {
                            if (fieldFilter.get(obj.getClass().getName()) != null) {
                                if (fieldFilter.get(obj.getClass().getName()).contains(f.getType().getName())) {
                                    continue;
                                }
                            }
                            if (isDebug(null, null, f)) {
                                logger.info("debug field: " + debugField);
                            }
                            Object returnObj = runGetter(f, obj);
                            if (returnObj == null) {
                                if (showNull) bud.append(indent + f.getName() + " is null!" + LINE_SEPARATOR, 1);
                                continue;
                            }
                            String value = null;
                            if (returnObj instanceof List) {
                                List<?> list = (List<?>) returnObj;
                                bud = printList(bud, indent, f.getName(), list);
                            } else if (returnObj.getClass().isArray()) {
                                bud = printArray(bud, indent, f.getName(), returnObj);
                            } else if (returnObj instanceof Set) {
                                Set<?> set = (Set<?>) returnObj;
                                bud = printSet(bud, indent, f.getName(), set);
                            } else if (returnObj instanceof Map) {
                                Map<?, ?> map = (Map<?, ?>) returnObj;
                                bud = printMap(bud, indent, f.getName(), map);
                            } else {
                                if (allowType(f.getType())) {
                                    value = getObjectString(returnObj);
                                    if (showFieldType) {
                                        bud.append(indent + f.getName() + "[" + f.getType().getName() + "] = " + value + LINE_SEPARATOR, value.hashCode());
                                    } else {
                                        bud.append(indent + f.getName() + " = " + value + LINE_SEPARATOR, value.hashCode());
                                    }
                                } else {
                                    bud = printObjectParam(bud, indent, f.getName(), returnObj);
                                }
                            }
                        }
                        cls = cls.getSuperclass();
                    }
                }
            }
            return bud;
        } catch (Exception e) {
            logger.severe("printObjectParam error objName: " + objName + ", obj class: " + obj.getClass().getName());
            throw new RuntimeException(e);
        }
    }
    
    private boolean isFilter(String name) {
        if (filterType.contains(name)) {
            return true;
        }
        for (String type : filterType) {
            if (name.startsWith(type)) {
                return true;
            }
        }
        return false;
    }

    private String getObjectString(Object obj) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        if (obj.getClass() == Date.class) {
            Date dt = (Date) obj;
            return sdf.format(dt);
        } else if (obj.getClass() == Timestamp.class) {
            Timestamp tsp = (Timestamp) obj;
            return sdf.format(new Date(tsp.getTime()));
        } else if (obj.getClass() == java.sql.Date.class) {
            java.sql.Date dt = (java.sql.Date) obj;
            return sdf.format(dt);
        } else if (obj.getClass() == String.class) {
            String result = removeLineBreak(obj.toString());
            if (encloseString) {
                return "'" + result + "'";
            } else {
                return result;
            }
        } else {
            return obj.toString();
        }
    }
    
    private String removeLineBreak(String str) {
        char br = (char) 10;
        char ret = (char) 13;
        if (str.indexOf(br) == -1 && str.indexOf(ret) == -1) {
            return str;
        }
        StringBuilder bud = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            int ascii = (int) c;
            if (ascii != 10 && ascii != 13) {
                bud.append(c);
            }
        }
        return bud.toString();
    }

    private boolean allowType(Class<?> cls) {
        if (cls == String.class) {
            return true;
        } else if (cls == Integer.class) {
            return true;
        } else if (cls == Double.class) {
            return true;
        } else if (cls == Float.class) {
            return true;
        } else if (cls == Long.class) {
            return true;
        } else if (cls == Short.class) {
            return true;
        } else if (cls == BigInteger.class) {
            return true;
        } else if (cls == BigDecimal.class) {
            return true;
        } else if (cls == Boolean.class) {
            return true;
        } else if (cls == Date.class) {
            return true;
        } else if (cls == Timestamp.class) {
            return true;
        } else if (cls == java.sql.Date.class) {
            return true;
        }
        return false;
    }

    public void printlnClasssMessage(Object obj, String string) {
        printMessage(obj.getClass().getName() + ":  " + string);
    }

    public void printObjectParam(String objName, Object obj) {
        RecordBuffer bud = new RecordBuffer();
        bud = printObjectParam(bud, "", objName, obj);
        printMessage(bud.toString());
    }
    
    public void printParam(String paramName, String value) {
        printMessage(paramName + " = " + value);
    }
    
    public void printMarkMessage(String message) {
        printMessage(MARKER + message);
    }

    public void printObjectParam(Object obj) {
        RecordBuffer bud = new RecordBuffer();
        bud = printObjectParam(bud, "", "Object", obj);
        printMessage(bud.toString());
    }
    
    public int getNormalizedHashcode(Object obj) {
        RecordBuffer bud = new RecordBuffer();
        bud = printObjectParam(bud, "", "Object", obj);
        return bud.getHashcode();
    }

    public boolean isShowNull() {
        return showNull;
    }

    public void setShowNull(boolean showNull) {
        this.showNull = showNull;
    }

    public void addFilterType(String className){
        filterType.add(className);
    }
    
    public void addPrintType(String className){
        allowType.add(className);
    }

    public boolean isShowFieldType() {
        return showFieldType;
    }

    public void setShowFieldType(boolean showFieldType) {
        this.showFieldType = showFieldType;
    }

    public boolean isEncloseString() {
        return encloseString;
    }

    public void setEncloseString(boolean encloseString) {
        this.encloseString = encloseString;
    }

    public boolean isShowOutOfBuffer() {
        return showOutOfBuffer;
    }

    public void setShowOutOfBuffer(boolean showOutOfBuffer) {
        this.showOutOfBuffer = showOutOfBuffer;
    }

    public void addOverloadFilter(String className, String fieldName) {
        if (fieldFilter.get(className) == null) {
            List<String> values = new ArrayList<String>();
            values.add(fieldName);
            fieldFilter.put(className, values);
        } else {
            fieldFilter.get(className).add(fieldName);
        }
    }
    
    public boolean isDebug(Class<?> cls, String method, String field){
        if (cls != null && cls == debugCls) {
            return true;
        }
        if (method != null && !method.equals("") && method.equalsIgnoreCase(debugMethod)) {
            return true;
        }
        if (field != null && !field.equals("") && field.equalsIgnoreCase(debugField)) {
            return true;
        }
        return false;
    }
    
    public boolean isDebug(Class<?> cls, Method method, Field field){
        if (cls != null && cls == debugCls) {
            return true;
        }
        if (method != null && method.getName().equalsIgnoreCase(debugMethod)) {
            return true;
        }
        if (field != null && field.getName().equalsIgnoreCase(debugField)) {
            return true;
        }
        return false;
    }
    
}