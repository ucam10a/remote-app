package com.yung.remote;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultCaret;

import com.yung.remote.socket.Client;

public class MyJDialog extends JDialog {

    public static final String NEW_LINE = System.getProperty("line.separator");
    
    public static final int DEFAULT_WIDTH = 550;
    public static final int DEFAULT_HEIGHT = 310;
    
    private App app;
    
    private Point p;
    
    private String clientIp;
    
    private static final long serialVersionUID = 1L;
    
    private JScrollPane scrollPane;
    
    private JTextArea log;
    
    private JTextArea msg;
    
    private JButton sendButton, fileButton;
    
    private MyActionListener listener = new MyActionListener();
    
    final MyJDialog dialog = this;
    
    private MessageLog msgLog;

    public MyJDialog(final App app, String title, final String clientIp, int offSet, final MessageLog msgLog) {
        super(app.getFrame(), title);
        this.app = app;
        this.clientIp = clientIp;
        this.msgLog = msgLog;
        
        // set the position of the window
        p = new Point(200 + offSet  * 30, 200 + offSet * 30);
        setLocation(p.x, p.y);

        // Create a message
        JPanel messagePane = new JPanel();
        log = new JTextArea(10, 46);
        log.setMargin(new Insets(5, 5, 5, 5));
        log.setEditable(false);
        DefaultCaret caret = (DefaultCaret) log.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        scrollPane = new JScrollPane(log);
        messagePane.add(scrollPane);
        
        // get content pane, which is usually the
        // Container of all the dialog's components.
        getContentPane().add(messagePane, BorderLayout.CENTER);

        // Create a text area
        msg = new JTextArea(3, 35);
        msg.setMargin(new Insets(5, 5, 5, 5));
        msg.setEditable(true);
        
        // add action listener to send message when press enter
        KeyStroke stroke = KeyStroke.getKeyStroke("ENTER");
        Action sendMsg = new AbstractAction() {
            private static final long serialVersionUID = 1L;
            public void actionPerformed(ActionEvent e) {
                if (clientIp == null || "".equals(clientIp.trim())) {
                    logMessage("Error: ip is null");
                    return;
                }
                String text = msg.getText();
                if (text == null || text.trim().equals("")) {
                    return;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd HH:mm:ss");
                String time = sdf.format(new Date());
                logMessage("Me [Time " + time + "]: \n" + msg.getText());
                msgLog.addLog("Me [Time " + time + "]: \n" + msg.getText());
                new Client(app, dialog).call("snedMsg", clientIp, msg.getText(), this);
                msg.setText("");
            }
        };
        InputMap inputMap = msg.getInputMap(JComponent.WHEN_FOCUSED);
        inputMap.put(stroke, "ENTER");
        msg.getActionMap().put("ENTER", sendMsg);
                
        sendButton = new JButton("send", null);
        sendButton.addActionListener(listener);
        
        fileButton = new JButton("file", null);
        fileButton.addActionListener(listener);
        
        JPanel msgTextPanel = new JPanel();
        msgTextPanel.setLayout(new FlowLayout());
        msgTextPanel.add(msg);
        msgTextPanel.add(sendButton);
        msgTextPanel.add(fileButton);
        
        this.addWindowListener(new WindowListener(){
            @Override
            public void windowActivated(WindowEvent arg0) {
            }
            @Override
            public void windowClosed(WindowEvent e) {
            }
            @Override
            public void windowClosing(WindowEvent e) {
                saveLogs();    
            }
            @Override
            public void windowDeactivated(WindowEvent e) {
            }
            @Override
            public void windowDeiconified(WindowEvent e) {
            }
            @Override
            public void windowIconified(WindowEvent e) {
            }
            @Override
            public void windowOpened(WindowEvent e) {
            }
        });
        
        
        // set action listener on the button
        getContentPane().add(msgTextPanel, BorderLayout.PAGE_END);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        dispose();
        
    }
    
    public void saveLogs() {
        String homeDir = System.getProperty("user.home");
        File logFile = FileUtil.getFile(homeDir + "/.msg/" + clientIp + ".txt");
        MarshalHelper hepler = new MarshalHelper();
        MessageLog log = null;
        if (logFile.exists()) {
            log = hepler.unmarshal(logFile, MessageLog.class);
        } else {
            log = new MessageLog();
        }
        log.getLogs().addAll(msgLog.getLogs());
        try {
            hepler.marshal(logFile.getParent(), clientIp, "log", log);
        } catch (IOException e1) {
        }
    }
    
    public Point getLoc() {
        return p;
    }
    
    public void initDialog() {
        setLocation(p);
    }

    public void showDialog() {
        setVisible(true);
    }

    public void hideDialog() {
        setVisible(false);
    }

    // override the createRootPane inherited by the JDialog, to create the rootPane.
    // create ~VUality to close the window when "Escape" button is pressed
    public JRootPane createRootPane() {
        JRootPane rootPane = new JRootPane();
        KeyStroke stroke = KeyStroke.getKeyStroke("ESCAPE");
        Action action = new AbstractAction() {
            
            private static final long serialVersionUID = 1L;

            public void actionPerformed(ActionEvent e) {
                System.out.println("escaping..");
                setVisible(false);
                dispose();
            }
        };
        InputMap inputMap = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(stroke, "ESCAPE");
        rootPane.getActionMap().put("ESCAPE", action);
        return rootPane;
    }

    public void logMessage(String message) {
        if (scrollPane != null) {
            log.append(message + NEW_LINE);
            JScrollBar vertical = scrollPane.getVerticalScrollBar();
            vertical.setValue(vertical.getMaximum());
            log.setCaretPosition(log.getDocument().getLength());
        }
    }
    
    public MessageLog getMsgLog() {
        return msgLog;
    }

    public void setMsgLog(MessageLog msgLog) {
        this.msgLog = msgLog;
    }

    // an action listener to be used when an action is performed
    // (e.g. button is pressed)
    class MyActionListener implements ActionListener {
        //close and dispose of the window.
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == sendButton) {
                if (clientIp == null || "".equals(clientIp.trim())) {
                    logMessage("Error: ip is null");
                    return;
                }
                String text = msg.getText();
                if (text == null || text.trim().equals("")) {
                    return;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/mm/dd HH:mm:ss");
                String time = sdf.format(new Date());
                logMessage("Me [Time " + time + "]: \n" + msg.getText());
                msgLog.addLog("Me [Time " + time + "]: \n" + msg.getText());
                new Client(app, dialog).call("snedMsg", clientIp, msg.getText(), this);
                msg.setText("");
            }
            if (e.getSource() == fileButton) {
                JFileChooser fc = new JFileChooser();
                int returnVal = fc.showOpenDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    final File f = fc.getSelectedFile();
                    if (f != null) {
                        final Client client = new Client(app, dialog);
                        ExecutorService executorService = Executors.newFixedThreadPool(1);
                        executorService.execute(new Runnable() {
                            public void run() {
                                try {
                                    client.call("sendFile", clientIp, f.getAbsolutePath(), this);
                                } catch (Exception e) {
                                    app.logMessage(e.toString());
                                }
                            }
                        });
                        executorService.shutdown();
                    }
                }
            }
        }
    }

    public void addMsgLog(String msg) {
        msgLog.addLog(msg);
    }
    
}