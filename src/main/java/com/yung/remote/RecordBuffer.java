package com.yung.remote;

import java.util.LinkedHashMap;
import java.util.logging.Logger;


/**
 * Customized String Buffer 
 * It will check input string to compare input to stored string
 * 
 * @author long
 *
 */
public class RecordBuffer {
    
    private Class<?> debugCls = AppLogger.getDebugCls();
    
    private boolean debug = false;
    
    private Logger logger = AppLogger.getLogger(RecordBuffer.class.getName());
    
    private int skipIndentSize = Integer.MAX_VALUE;
    
    private static int BUFFER_LIMIT = 3;
    
    private String[] checkBuffer = new String[BUFFER_LIMIT];
    private int[] hashcodeBuffer = new int[BUFFER_LIMIT];
    
    private int hashcode = 1;
    
    private StringBuilder bud = new StringBuilder();
    
    private LinkedHashMap<String, Integer> compareCache = new LinkedHashMap<String, Integer>();
    
    public RecordBuffer(Class<?> debugCls){
        if (debugCls != null && debugCls == this.debugCls) {
            this.debug = true;
        }
    }
    
    public RecordBuffer(){
    }
    
    /**
     * append string
     * 
     * @param line string
     */
    public void append(String line, int hashcode) {
        if (debug) logger.fine("line: " + line + ", hashcode: " + hashcode);
        logger.fine("line: " + line + ", hashcode: " + hashcode);
        if (BUFFER_LIMIT == 0) {
            bud.append(line);
            this.hashcode = this.hashcode * hashcode;
            return;
        }
        if (getIndentSize(line) >= skipIndentSize) {
            if (debug) logger.info("ignore: " + line);
            logger.fine("ignore: " + line);
            return;
        } else {
            skipIndentSize = Integer.MAX_VALUE;
        }
        if (!cacheExist(line.trim())) {
            if (!isBufferEmpty()) {
                appendCompareCache();
                appendBuffer();
                cleanBuffer();
            }
            addCache(line.trim());
            bud.append(line);
            this.hashcode = this.hashcode * hashcode;
        } else {
            if (!isBufferOverflow()) {
                insertBuffer(line, hashcode);
            } else {
                skipIndentSize = getIndentSize(checkBuffer[0]);
                cleanBuffer(); 
            }
        }
    }
    
    private void addCache(String line) {
        compareCache.put(line, compareCache.size());
    }

    private boolean cacheExist(String line) {
        if (compareCache.get(line) != null) {
            if (!isBufferEmpty()) {
                int idx = compareCache.get(line);
                int bufIdx = getLastBufferIndex();
                if (idx != (bufIdx + 1)) {
                    return false;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    private int getLastBufferIndex() {
        int index = 0;
        while (index < BUFFER_LIMIT) {
            if (checkBuffer[index] == null) {
                return index - 1;
            }
            index++;
        }
        return index - 1;
    }

    private void appendCompareCache() {
        for (int i = 0; i < BUFFER_LIMIT; i++) {
            if (checkBuffer[i] != null) {
                addCache(checkBuffer[i].trim());
            }
        }
    }

    private boolean isBufferEmpty() {
        if (checkBuffer[0] == null) {
            return true;
        } else {
            return false;
        }
    }

    private void insertBuffer(String line, int hashcode) {
        for (int i = 0; i < BUFFER_LIMIT; i++) {
            if (checkBuffer[i] == null) {
                checkBuffer[i] = line;
                hashcodeBuffer[i] = hashcode;
                break;
            }
        }
    }

    private boolean isBufferOverflow() {
        for (int i = 0; i < BUFFER_LIMIT; i++) {
            if (checkBuffer[i] == null) {
                return false;
            }
        }
        return true;
    }

    private int getIndentSize(String line) {
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) != ' ') {
                return i;
            }
        }
        return Integer.MAX_VALUE;
    }

    private void cleanBuffer() {
        for (int i = 0; i < BUFFER_LIMIT; i++) {
            if (checkBuffer[i] != null) {
                checkBuffer[i] = null;
                hashcodeBuffer[i] = 1;
                if (debug) logger.info("ignore line: " + checkBuffer[i]);
            }
        }
    }
    
    /**
     * output string
     */
    public String toString(){
        appendBuffer();
        cleanBuffer();
        return bud.toString();
    }

    private void appendBuffer() {
        for (int i = 0; i < BUFFER_LIMIT; i++) {
            if (checkBuffer[i] != null) {
                bud.append(checkBuffer[i]);
                this.hashcode = this.hashcode * hashcodeBuffer[i];
            }
        }
    }
    
    /**
     * set compare buffer size
     * 
     * @param size buffer size
     */
    public void setBufferSize(int size){
        BUFFER_LIMIT = size;
    }
    
    /**
     * get normalized hashcode
     * 
     * @return normalized hashcode
     */
    public int getHashcode(){
    	return hashcode;
    }

}