package com.yung.remote;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * For zip file
 * 
 * @author Yung Long
 * 
 */
public class AppZipManager {

    private static Logger logger = Logger.getLogger(AppZipManager.class.getName());

    /**
     * zip folder to a zip file
     * 
     * @param source_folder
     *            source folder path
     * @param output_zip_file
     *            output file path
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     */
    public void zipFile(String source_folder, String output_zip_file) throws URISyntaxException, UnsupportedEncodingException {
        List<String> fileList = new ArrayList<String>();
        generateFileList(FileUtil.getFileURI(source_folder), FileUtil.getFile(source_folder), fileList);
        zipIt(FileUtil.getFileURI(source_folder), FileUtil.getFileURI(output_zip_file), fileList);
    }

    /**
     * zip files to a zip file
     * 
     * @param files
     *            file array
     * @param output_zip_file
     *            output file path
     * @throws URISyntaxException
     */
    public void zipFile(File[] files, String[] fileFir, File output_zip_file) throws URISyntaxException {
        zipIt(files, fileFir, output_zip_file);
    }

    /**
     * zip function
     * 
     * @param source_folder
     *            zip source folder path
     * @param output_zip_file
     *            output file path
     * @param fileList
     *            collecting file list container
     * @throws URISyntaxException
     */
    private void zipIt(String source_folder, String output_zip_file, List<String> fileList) throws URISyntaxException {

        byte[] buffer = new byte[1024];

        try {
            FileOutputStream fos = new FileOutputStream(FileUtil.getFile(output_zip_file));
            ZipOutputStream zos = new ZipOutputStream(fos);
            logger.info("Output to Zip : " + output_zip_file);
            for (String file : fileList) {
                logger.info("File Added : " + file);
                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(FileUtil.getFile(source_folder + "/" + file));
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            // remember close it
            zos.close();
            logger.info("Done");
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.toString(), e);
        }
    }

    /**
     * zip function
     * 
     * @param files
     *            source file array
     * @param output_zip_file
     *            output zip file path
     * @throws URISyntaxException
     */
    private void zipIt(File[] files, String[] fileDir, File output_zip_file) throws URISyntaxException {
        byte[] buffer = new byte[1024];
        try {
            FileOutputStream fos = new FileOutputStream(output_zip_file);
            ZipOutputStream zos = new ZipOutputStream(fos);
            logger.info("Output to Zip : " + output_zip_file);
            for (int i = 0; i < files.length; i++) {
                logger.info("File Added : " + files[i]);
                ZipEntry ze = new ZipEntry(fileDir[i] + files[i].getName());
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(files[i]);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            // remember close it
            zos.close();
            logger.info("Done");
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.toString(), e);
        }
    }

    /**
     * recursive to get the all sub folder sile
     * 
     * @param source_folder
     *            source folder path
     * @param node
     *            current folder
     * @param fileList
     *            collecting file list container
     */
    public void generateFileList(String source_folder, File node, List<String> fileList) {
        // add file only
        if (node.isFile()) {
            fileList.add(generateZipEntry(FileUtil.getFileURI(source_folder), node.toURI().toString()));
        }
        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(FileUtil.getFileURI(source_folder), new File(node, filename), fileList);
            }
        }
    }

    /**
     * get sub folder path to zip file with a tree hierarchy
     * 
     * @param source_folder
     *            parent folder path
     * @param file
     *            sub folder name
     * @return sub folder path
     */
    private String generateZipEntry(String source_folder, String file) {
        return file.substring(source_folder.length(), file.length());
    }

    /**
     * generate file map, path is key, file is value
     * 
     * @param node
     *            file
     * @param fileMap
     *            file map
     */
    public static void generateFileMap(File node, Map<String, File> fileMap) {
        // add file only
        if (node.isFile()) {
            fileMap.put(generateFilePath(FileUtil.getFileURI(""), node.toURI().toString()), node);
        }
        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileMap(new File(node, filename), fileMap);
            }
        }
    }

    /**
     * get file path
     * 
     * @param source_folder
     *            source folder path
     * @param file
     *            file
     * @return file path under source folder
     */
    private static String generateFilePath(String source_folder, String file) {
        return file.substring(source_folder.length(), file.length());
    }

    /**
     * upzip file
     * 
     * @param zipFile
     *            zip file
     * @param outputFolder
     *            output folder path
     * @return folder file
     * @throws IOException
     */
    public File unZipIt(String zipFile, String outputFolder) throws IOException {
        byte[] buffer = new byte[1024];
        File folder = null;
        // create output directory is not exists
        folder = new File(FileUtil.getFileURI(outputFolder));
        if (folder.exists()) {
            folder.delete();
            folder.mkdir();
        } else {
            folder.mkdir();
        }
        // get the zip file content
        ZipInputStream zis = new ZipInputStream(new FileInputStream(FileUtil.getFile(zipFile).getAbsolutePath()));
        // get the zipped file list entry
        ZipEntry ze = zis.getNextEntry();
        while (ze != null) {
            String fileName = ze.getName();
            boolean isDirectory = ze.isDirectory();
            File newFile = FileUtil.getFile(outputFolder + File.separator + fileName);
            logger.info("file unzip : " + newFile.getAbsoluteFile());
            // create all non exists folders
            // else you will hit FileNotFoundException for compressed folder
            if (isDirectory) {
                if (newFile.exists()) {
                    newFile.delete();
                    newFile.mkdir();
                } else {
                    newFile.mkdir();
                }
            } else {
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            ze = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
        logger.info("Done");
        return folder;
    }

    /**
     * test method or ant command
     * 
     * @param args
     * @throws URISyntaxException
     * @throws IOException
     */
    public static void main(String[] args) throws URISyntaxException, IOException {

        if (args.length == 0) {
            args = new String[2];
            args[0] = "file:/E:/diff";
            args[1] = "file:/E:/git/diff.zip";
        }
        
        AppZipManager zipmgr = new AppZipManager();
        zipmgr.zipFile(args[0], args[1]);
        zipmgr.unZipIt(args[1], "file:/E:/land/diff");

    }

}