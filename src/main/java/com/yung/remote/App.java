package com.yung.remote;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.yung.remote.W32API.HWND;
import com.yung.remote.socket.Client;
import com.yung.remote.socket.Server;

public class App extends JPanel implements ActionListener {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public static final String NEW_LINE = System.getProperty("line.separator");
    private JFrame frame;
    JButton serverButton, clientButton, stopButton, requestButton, sendButton, callButton;
    protected JScrollPane scrollPane;
    private JTextField ipField;
    private JPasswordField pwdField;
    protected JTextArea log;
    private JComboBox<ComboItem> comboBox;
    private Map<String, String> contactMap = new HashMap<String, String>(); // key: nickname, value: machine name
    private Map<String, String> ipMap = new HashMap<String, String>();  // key ip, value: nickname
    private Map<String, String> nameIpMap = new HashMap<String, String>();  // key nickname, value: ip
    private Map<String, MyJDialog> dislogMap = new HashMap<String, MyJDialog>();  // key ip, value: dialog
    
    private boolean focus = false;
    private boolean message = false;
    private boolean blink = false;
    private List<MyJDialog> waitDailogs = new ArrayList<MyJDialog>();
    
    public App() {
        super(new BorderLayout());
        init();
    }
    
    public synchronized void showDialog(MyJDialog dialog) {
        if (!getFocus()) {
            waitDailogs.add(dialog);
            if (!getBlink()) {
                blink();
            }
        } else {
            dialog.showDialog();
        }
    }
    
    private synchronized void showWaitDialogs() {
        if (waitDailogs.size() == 0) return; 
        for (MyJDialog dialog :  waitDailogs) {
            dialog.showDialog();
        }
        waitDailogs.clear();
        setMessage(false);
        setBlink(false);
    }
    
    public synchronized boolean getFocus() {
        return focus;
    }
    
    public synchronized void setFocus(boolean focus) {
        this.focus = focus;
    }
    
    public synchronized boolean getBlink() {
        return blink;
    }
    
    public synchronized void setBlink(boolean blink) {
        this.blink = blink;
    }
    
    public synchronized boolean hasMessage() {
        return message;
    }
    
    public synchronized void setMessage(boolean message) {
        this.message = message;
    }
    
    public Map<String, String> getContactMap() {
        return contactMap;
    }
    
    public Map<String, String> getIpMap() {
        return ipMap;
    }
    
    public Map<String, MyJDialog> getDislogMap() {
        return dislogMap;
    }
    
    public JFrame getFrame() {
        return frame;
    }
    
    public JTextField getIpField() {
        return ipField;
    }
    
    public JTextField getPwdField() {
        return pwdField;
    }
    
    public void init() {
        
        focus = true;
        log = new JTextArea(30, 35);
        log.setMargin(new Insets(5, 5, 5, 5));
        log.setEditable(false);
        scrollPane = new JScrollPane(log);
        
        serverButton = new JButton("server", null);
        serverButton.addActionListener(this);
        
        clientButton = new JButton("client", null);
        clientButton.addActionListener(this);
        
        stopButton = new JButton("stop", null);
        stopButton.addActionListener(this);
        
        requestButton = new JButton("request", null);
        requestButton.addActionListener(this);
        
        sendButton = new JButton("send", null);
        sendButton.addActionListener(this);
        
        callButton = new JButton("call", null);
        callButton.addActionListener(this);
        
        setCombo();
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        buttonPanel.add(serverButton);
        buttonPanel.add(clientButton);
        buttonPanel.add(stopButton);
        buttonPanel.add(requestButton);
        buttonPanel.add(callButton);
        buttonPanel.add(comboBox);
        
        JLabel ipLabel = new JLabel("ip");
        ipField = new JTextField("", 12);
        
        JLabel pwdLabel = new JLabel("pwd");
        pwdField = new JPasswordField("", 8);
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new FlowLayout());
        textPanel.add(ipLabel);
        textPanel.add(ipField);
        textPanel.add(pwdLabel);
        textPanel.add(pwdField);
        
        JPanel msgTextPanel = new JPanel();
        msgTextPanel.setLayout(new FlowLayout());
        msgTextPanel.add(sendButton);
        
        JPanel topPanel = new JPanel();
        topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
        topPanel.add(textPanel);
        topPanel.add(buttonPanel);
        
        //Add the buttons and the log to this panel.
        add(topPanel, BorderLayout.PAGE_START);
        add(scrollPane, BorderLayout.CENTER);
        add(msgTextPanel, BorderLayout.PAGE_END);
        String homeDir = System.getProperty("user.home");
        File config = FileUtil.getFile(homeDir + "/.msg/msg-contact.properties");
        logMessage("============== Author: Yung Long Li, MIT License");
        logMessage("Config file: " + config.getAbsolutePath());
        focus = true;
        
    }
    
    private void setCombo() {
        try {
            String homeDir = System.getProperty("user.home");
            File config = FileUtil.getFile(homeDir + "/.msg/msg-contact.properties");
            FileUtil.generateDir(config.getAbsolutePath());
            if (config.exists()) {
                InputStream  in = new FileInputStream(config);
                Properties prop = new Properties();
                prop.load(in);
                for (Object key : prop.keySet()) {
                    contactMap.put(key.toString(), prop.getProperty(key.toString()));
                }
                for (Entry<String, String> entry : contactMap.entrySet()) {
                    InetAddress addr = InetAddress.getByName(entry.getValue());
                    String ip = addr.getHostAddress();
                    ipMap.put(ip, entry.getKey());
                    nameIpMap.put(entry.getKey(), ip);
                }
            }
            comboBox = new JComboBox<ComboItem>();
            comboBox.addActionListener(this);
            comboBox.addItem(new ComboItem("    "));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == serverButton) {
            if (checkZeroremoteProcess()) {
                breakConnection();
            }
            setPass();
        }
        if (e.getSource() == clientButton) {
            if (checkZeroremoteProcess()) {
                breakConnection();
            }
            connect();
        }
        if (e.getSource() == stopButton) {
            if (checkZeroremoteProcess()) {
                closeZeroRemote();
            }
        }
        if (e.getSource() == requestButton) {
            if (checkZeroremoteProcess()) {
                breakConnection();
            }
            new Client(this, null).call("connectToServer");
        }
        if (e.getSource() == sendButton) {
            String name = comboBox.getSelectedItem().toString();
            if (name.endsWith("*")) {
                name = name.substring(0, name.length() - 2);
            }
            String clientIp = nameIpMap.get(name);
            if (clientIp == null || "".equals(clientIp.trim())) {
                logMessage("Error: ip is null");
                return;
            }
            if (dislogMap.get(name) == null) {
                String homeDir = System.getProperty("user.home");
                File logFile = FileUtil.getFile(homeDir + "/.msg/" + clientIp + ".txt");
                MarshalHelper hepler = new MarshalHelper();
                MessageLog log = null;
                if (logFile.exists()) {
                    log = hepler.unmarshal(logFile, MessageLog.class);
                } else {
                    log = new MessageLog();
                }
                final App app = this;
                MyJDialog dialog = new MyJDialog(app, name, clientIp, dislogMap.size(), log);
                // set previous ten logs
                String msgLogs = log.getLastLogs();
                dialog.logMessage(msgLogs);
                // set the size of the window
                dialog.setSize(MyJDialog.DEFAULT_WIDTH, MyJDialog.DEFAULT_HEIGHT);
                dialog.showDialog();
                dislogMap.put(name, dialog);
            } else {
                dislogMap.get(name).showDialog();
            }
        }
        if (e.getSource() == callButton) {
            String clientIp = ipField.getText();
            if (clientIp == null || "".equals(clientIp.trim())) {
                logMessage("Error: ip is null");
                return;
            }
            new Client(this, null).call("alert");
            logMessage("===== alert to " + ipField.getText());
        }
        if (e.getSource() == comboBox) {
            @SuppressWarnings("rawtypes")
            JComboBox cb = (JComboBox)e.getSource();
            ComboItem contactObj = (ComboItem) cb.getSelectedItem();
            String contact = contactObj.toString();
            if (contact.toString().endsWith("*")) {
                contact = contact.substring(0, contact.length() - 2);
            }
            String ip = contactMap.get(contact);
            if (ip != null) ipField.setText(ip);
        }
    }
    
    private void createAndShowGUI() {
        //Create and set up the window.
        frame = new JFrame("Messager");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        //Add content to the window.
        frame.add(this);
        
        //Display the window.
        frame.getContentPane().setPreferredSize(new Dimension(500, 650));
        frame.pack();
        frame.setVisible(true);
        
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowActivated(WindowEvent arg0) {
            }
            @Override
            public void windowClosed(WindowEvent arg0) {
            }
            @Override
            public void windowClosing(WindowEvent arg0) {
                for (MyJDialog dialog : dislogMap.values()) {
                    dialog.saveLogs();
                }
            }
            @Override
            public void windowDeactivated(WindowEvent arg0) {
            }
            @Override
            public void windowDeiconified(WindowEvent arg0) {
                showWaitDialogs();
                setFocus(true);
            }
            @Override
            public void windowIconified(WindowEvent arg0) {
                setFocus(false);
            }
            @Override
            public void windowOpened(WindowEvent arg0) {
            }
        });
        
        // add close event
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                closeZeroRemote();
            }
        });
        
        // set icon
        InputStream in = App.class.getClassLoader().getResourceAsStream("icon.gif");
        byte[] bytes = toByteArray(in);
        Image icon = Toolkit.getDefaultToolkit().createImage(bytes);
        frame.setIconImage(icon);
        
        // start socket listener
        final App app = this;
        
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new Runnable() {
            public void run() {
                new Server(app);
            }
        });
        executorService.execute(new Runnable() {
            public void run() {
                Client client = new Client(app, null);
                Map<String, Integer> idxMap = new HashMap<String, Integer>();
                Set<String> list = new HashSet<String>();
                for (String name : nameIpMap.keySet()) {
                    list.add(name);
                }
                boolean init = true;
                logMessage("contacts size " + nameIpMap.size());
                if (nameIpMap.size() == 0) {
                    String homeDir = System.getProperty("user.home");
                    File config = FileUtil.getFile(homeDir + "/.msg/msg-contact.properties");
                    if (!config.exists()) {
                        logMessage("msg-contact.properties not found!");
                    }
                }
                while (true) {
                    for (Entry<String, String> entry : ipMap.entrySet()) {
                        String ip = entry.getKey();
                        String nickName = entry.getValue();
                        boolean isAlive = client.checkAlive(ip);
                        if (isAlive) {
                            if (init) {
                                comboBox.addItem(new ComboItem(nickName + " *"));
                            } else {
                                int removeIdx = idxMap.get(ip);
                                comboBox.getItemAt(removeIdx).setContent(nickName + " *");
                            }
                        } else {
                            if (init) {
                                comboBox.addItem(new ComboItem(nickName));
                            } else {
                                int removeIdx = idxMap.get(ip);
                                comboBox.getItemAt(removeIdx).setContent(nickName);
                            }
                        }
                        refreshMap(comboBox, idxMap, list);
                    }
                    try {
                        Thread.sleep(30000);
                    } catch (Exception e) {
                    }
                    init = false;
                }
            }
        });
        executorService.shutdown();
        
    }
    
    private void blink() {
        if (getBlink()) {
            return;
        }
        setBlink(true);
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(new Runnable() {
            public void run() {
                try {
                    while (!getFocus() && hasMessage()) {
                        User32 user32 =  User32.INSTANCE;
                        HWND hwnd = user32.FindWindow(0, "Messager");
                        user32.FlashWindow(hwnd, true);
                        Thread.sleep(2000);
                    }
                } catch (Exception e) {
                }
            }
        });
        executorService.shutdown();
    }

    protected void refreshMap(JComboBox<ComboItem> combo, Map<String, Integer> idxMap, Set<String> list) {
        idxMap.clear();
        list.clear();
        for (int i = 0; i < 100; i++) {
            Object obj = combo.getItemAt(i);
            if (obj != null) {
                String itemName = obj.toString();
                if (itemName.endsWith("*")) {
                    itemName = itemName.substring(0, itemName.length() - 2);
                }
                String ip = nameIpMap.get(itemName);
                idxMap.put(ip, i);
                list.add(itemName);
            }
        }
    }

    protected File setup() {
        try {
            String tempDir = System.getProperty("java.io.tmpdir");
            String homeDir = System.getProperty("user.home");
            File zeroremote = FileUtil.getFile(homeDir + "/.zeroremote/zeroremote.exe");
            if (!zeroremote.exists()) {
                logMessage("zeroremote not exist, initialize ...");
                FileUtil.generateDir(zeroremote.getAbsolutePath());
                File jar = new File(App.class.getProtectionDomain().getCodeSource().getLocation().getPath());
                File tempJar = FileUtil.getFile(tempDir + "/" + jar.getName());
                FileUtil.fileCopy(jar, tempJar.getAbsolutePath());
                AppZipManager zipmgr = new AppZipManager();
                zipmgr.unZipIt(tempJar.getAbsolutePath(), tempDir + "/zeroremote");
                File temp = FileUtil.getFile(tempDir + "/zeroremote/zeroremote.exe");
                FileUtil.fileCopy(temp, zeroremote.getAbsolutePath());
                logMessage("Initialization done");
            }
            return zeroremote;
        } catch (Exception e) {
            logMessage(e.toString());
            throw new RuntimeException(e);
        }
    }
    
    protected String createRandomPass(int digit) {
        Random ran = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < digit; i++) {
            int num = ran.nextInt(10);
            sb.append(num + "");
        }
        return sb.toString();
    }
    
    private void closeZeroRemote() {
        try {
            Runtime.getRuntime().exec("cmd /c taskkill /im zeroremote.exe /f");
            logMessage("break current connection!");
        } catch (Exception e) {
            logMessage(e.toString());
            throw new RuntimeException(e);
        }
    }
    
    public void breakConnection() {
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(new Runnable() {
            public void run() {
                if (checkZeroremoteProcess()) {
                    closeZeroRemote();    
                }
            }
        });
        executorService.shutdown();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            logMessage(e.toString());
        }
    }
    
    public void setPass() {
        try {
            if (checkZeroremoteProcess()) {
                closeZeroRemote();
            } else {
                File zeroremote = setup();
                char[] pwdChars = pwdField.getPassword();
                String pwd = new String(pwdChars);
                if (pwd == null || pwd.trim().equals("")) {
                    pwd = createRandomPass(7);
                }
                Runtime.getRuntime().exec("cmd /c \"" + zeroremote.getAbsolutePath() + "\" /s /en /pw:" + pwd);
                String ip = InetAddress.getLocalHost().getHostAddress();
                logMessage("host ip: " + ip);
                logMessage("pwd: " + pwd);
                logMessage("server started!");
            }
        } catch (Exception e) {
            logMessage(e.toString());
            throw new RuntimeException(e);
        }
    }
    
    public void connect() {
        try {
            if (checkZeroremoteProcess()) {
                closeZeroRemote();
                logMessage("break current connection!");
            } else {
                File zeroremote = setup();
                String ip = ipField.getText();
                char[] pwdChars = pwdField.getPassword();
                String pwd = new String(pwdChars);
                if (!ip.contains(".")) {
                    InetAddress addr = InetAddress.getByName(ip);
                    ip = addr.getHostAddress();
                }
                if (pwd == null || pwd.trim().equals("")) {
                    pwd = createRandomPass(7);
                }
                String cmd = "\"" + zeroremote.getAbsolutePath() + "\" /c:" + ip + " /en /pw:" + pwd;
                Runtime.getRuntime().exec(cmd);
                logMessage("client connect successfully!");
            }
        } catch (Exception e) {
            logMessage(e.toString());
            throw new RuntimeException(e);
        }
    }
    
    public boolean checkZeroremoteProcess() {
        try {
            Process p = Runtime.getRuntime().exec("tasklist");
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = null;
            line = in.readLine();
            while (line != null) {
                if (line.trim().startsWith("zeroremote.exe")) {
                    return true;
                }
                line = in.readLine();
            }
            return false;
        } catch (Exception e) {
            logMessage(e.toString());
            throw new RuntimeException(e);
        }
    }

    private byte[] toByteArray(InputStream in) {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[16384];
            while ((nRead = in.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            return buffer.toByteArray();    
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void run() {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //Turn off metal's use of bold fonts
                UIManager.put("swing.boldMetal", Boolean.FALSE);
                createAndShowGUI();
            }
        });
    }
    
    public void logMessage(String message) {
        if (scrollPane != null) {
            log.append(message + NEW_LINE);
            JScrollBar vertical = scrollPane.getVerticalScrollBar();
            vertical.setValue(vertical.getMaximum());
        }
    }
    
    public static void main(String args[]) throws Exception {
        
        App app = new App();
        app.run();
        
    }
    
}