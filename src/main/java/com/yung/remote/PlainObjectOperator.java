package com.yung.remote;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.logging.Logger;

/**
 * to get/set value from/to object
 * 
 * @author Yung Long Li
 *
 */
public class PlainObjectOperator {

    private static final Logger logger = Logger.getLogger(PlainObjectOperator.class.getName());
    
    public static Object runGetter(Object obj, String fieldName) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Field f = getField(obj.getClass(), fieldName);
        return runGetter(f, obj);
    }
    
    public static Object runGetter(Field field, Object obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Method method = null;
        String type = field.getType().getName();
        if (type.equalsIgnoreCase("boolean")) {
            method = findBooleanGetter(field.getName(), obj);
        } else {
            method = findGetter(field.getName(), obj);
        }
        if (method != null) {
            return method.invoke(obj);
        }
        int modifier = field.getModifiers();
        if (!Modifier.isFinal(modifier) && !Modifier.isStrict(modifier)) {
            field.setAccessible(true);
            return field.get(obj);    
        }
        return null;
    }
    
    private static Method findGetter(String fieldName, Object obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (fieldName.length() + 3))) {
                if (method.getName().toLowerCase().endsWith(fieldName.toLowerCase())) {
                    if (method.getParameterTypes().length == 0) {
                        return method;
                    }
                }
            }
        }
        return null;
    }
    
    private static Method findBooleanGetter(String fieldName, Object obj) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().startsWith("is")) && (method.getName().length() == (fieldName.length() + 2))) {
                if (method.getName().toLowerCase().endsWith(fieldName.toLowerCase())) {
                    return method;
                }
            }
        }
        return null;
    }
    
    public static <T> T runGetter(Object obj, String fieldName, Class<T> type) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (fieldName.length() + 3))) {
                if (method.getName().toLowerCase().endsWith(fieldName.toLowerCase())) {
                    Object ret = method.invoke(obj);
                    if (ret == null) return null;
                    if (type.isAssignableFrom(ret.getClass())) {
                        return type.cast(ret);
                    } else {
                        throw new RuntimeException("type is incompatible!");
                    }
                }
            }
        }
        return null;
    }
    
    public static void runSetter(Object obj, String fieldName, Object value) throws Exception {
        boolean isSuccess = setObject(obj, fieldName, value);
        if (!isSuccess) {
            Field f = getField(obj.getClass(), fieldName);
            if (f != null) {
                int modifier = f.getModifiers();
                if (!Modifier.isFinal(modifier) && !Modifier.isStrict(modifier)) {
                    f.setAccessible(true);
                    f.set(obj, value);    
                }    
            }
        }
    }
    
    private static Field getField(Class<?> cls, String fieldName) {
        while (cls != null) {
            Field[] fields = cls.getDeclaredFields();
            for (Field f : fields) {
                if (f.getName().equals(fieldName)) {
                    return f;
                }
            }
            cls = cls.getSuperclass();
        }
        return null;
    }

    private static boolean setObject(Object obj, String fieldName, Object value) throws Exception {
        for (Method method : obj.getClass().getMethods()) {
            if ((method.getName().startsWith("set")) && (method.getName().length() == (fieldName.length() + 3))) {
                if (method.getName().toLowerCase().endsWith(fieldName.toLowerCase())) {
                    Type[] types = method.getGenericParameterTypes();
                    if (types.length > 1) {
                        logger.fine("setter can have only one type! skip this method: set" + fieldName);
                        continue;
                    }
                    boolean checked = true;
                    String typeName = types[0].toString();
                    int index = typeName.indexOf("<");
                    if (index != -1) {
                        String type = typeName.substring(0, index);
                        if (!Class.forName(type).isAssignableFrom(value.getClass())) {
                            logger.warning("type: " + value.getClass().getName() + " can not cast to " + type);
                            checked = false;
                            break;
                        }
                    } else {
                        if (!getClass(types[0].toString()).isAssignableFrom(value.getClass())) {
                            if (!typeName.contains(value.getClass().getName())) {
                                logger.warning("type: " + typeName + " not equal to " + value.getClass().getName());
                                checked = false;
                                break;
                            } 
                        }
                    }
                    if (checked) {
                        method.invoke(obj, value);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    private static Class<?> getClass(String type) throws ClassNotFoundException {
        if (type.equalsIgnoreCase("int")) {
            return Integer.class;
        } else if (type.equalsIgnoreCase("double")) {
            return Double.class;
        } else if (type.equalsIgnoreCase("float")) {
            return Float.class;
        } else if (type.equalsIgnoreCase("long")) {
            return Long.class;
        } else if (type.equalsIgnoreCase("boolean")) {
            return Boolean.class;
        } else {
            int index = type.indexOf("class");
            if (index != -1) {
                type = type.substring("class".length()).trim();
            }
            return Class.forName(type);
        }
    }

}