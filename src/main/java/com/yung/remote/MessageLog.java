package com.yung.remote;

import java.util.ArrayList;
import java.util.List;

public class MessageLog {

    private List<String> logs = new ArrayList<String>();

    public List<String> getLogs() {
        return logs;
    }

    public void setLogs(List<String> logs) {
        this.logs = logs;
    }

    public void addLog(String msg) {
        logs.add(msg);
    }

    public String getLastLogs() {
        StringBuilder sb = new StringBuilder();
        if (logs.size() > 10) {
            int start = logs.size() - 10;
            for (int i = start; i < logs.size(); i++) {
                String log = logs.get(i);
                log = log.replaceAll("]:", "]:\n");
                sb.append(log + "\n");
            }
        } else {
            for (String log : logs) {
                log = log.replaceAll("]:", "]:\n");
                sb.append(log + "\n");
            }
        }
        sb.append("========================\n");
        return sb.toString();
    }
    
}
